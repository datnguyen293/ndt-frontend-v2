<?php

error_reporting(E_ALL);

/**
 * Change working directory to 1-up-level
 */
chdir(dirname(__DIR__));

define('ROOT', dirname(__DIR__));
define('BASE_PATH', dirname(__DIR__));
define('APPLICATION_PATH', BASE_PATH . '/app');
define('PRODUCTION', 'production');
define('DEVELOPMENT', 'development');
define('DESKTOP_LAYOUT', 'desktop');
define('MOBILE_LAYOUT', 'mobile');

/**
 * Get environment by ordering
 *  - Read environment from $_SERVER['APPLICATION_ENV']
 *  - Else read it from hosting-environment with getenv('APPLICATION_ENV')
 *  - If nothing set, 'production' will be default environment
 */
$applicationEnv = PRODUCTION;
if (!empty($_SERVER['APPLICATION_ENV'])) {
    $applicationEnv = $_SERVER['APPLICATION_ENV'];
}
else if (!empty(getenv('APPLICATION_ENV'))) {
    $applicationEnv = getenv('APPLICATION_ENV');
}
else {
    $applicationEnv = PRODUCTION;
}
define('APPLICATION_ENV', $applicationEnv);

/**
 * Run Bootstrap file
 */
require_once APPLICATION_PATH . '/Bootstrap.php';
$bootstrap = new \Ixosoftware\Cms\Bootstrap();
$bootstrap->run();
