# README #

Hướng dẫn cài đặt và lập trình

### Cài đặt ###

* Clone source code từ bitbucket về và xem hướng dẫn cài đặt của Phalconphp
* https://docs.phalconphp.com/en/3.2/installation
* 
* Sau đó chạy `composer install`
* Cần cài đặt memcached
* Cần cài đặt APC `pecl install apcu_bc`

### Cấu hình ###

* Hệ thống sẽ tự động đọc biến `APPLICATION_ENV` từ `$_SERVER['APPLICATION_ENV']` hoặc `getenv('APPLICATION_ENV')` để set vào biến `APPLICATION_ENV`
* Tùy thuộc giá trị của biến này là 'development' hay 'production', hệ thống sẽ đọc configuration từ file tương ứng
  ```
  app/config/environment/development.php  
  ```
  ```
  app/config/environment/production.php
  ```
* 


### Cấu trúc views ###

* Views nằm trong folder `app/views`, mỗi controller có một folder riêng với tên được biến thành lowercase và calmel sẽ thành '-', ví dụ:
* `ErrorHandlerController` sẽ có folder views là `error-handler`
* Hệ thống tự động nhận diện mobile/desktop tùy thuộc vào user-agent của người dùng. Mỗi loại sẽ có file layout riêng nằm trong folder `app/views/layouts`

* Mỗi action trong controller sẽ có 2 file views riêng, một cho desktop và một cho mobile.
* Ví dụ: indexAction() trong IndexController sẽ có 2 file views  
  ``` 
  app/views/index/index.phtml
  ```  
  ```
  app/views/index/m.index.phtml
  ```
* 
