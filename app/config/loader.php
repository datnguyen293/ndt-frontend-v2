<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader
    ->registerNamespaces([
        'Ixosoftware\Cms\Controllers' => $config->application->controllersDir,
        'Ixosoftware\Cms\Models' => $config->application->modelsDir,
        'Ixosoftware\Cms\Plugins' => $config->application->pluginsDir,
        'Ixosoftware\Cms\Helpers' => $config->application->helpersDir
    ])
    ->registerDirs([
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->helpersDir
    ])
    ->registerFiles([
        $config->application->vendorsDir . '/autoload.php'
    ])
    ->register();
