<?php

use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up shared session
 */
$di->setShared('session', function() {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

/**
 * Setting up profiler
 */
$di->setShared('profiler', function() {
    return new Phalcon\Db\Profiler();
});

/**
 * Setting up the view component
 */
$di->setShared('view', function() use ($config) {
    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.phtml' => PhpEngine::class
    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 * Read database
 */
$di->setShared('db', function() use ($config) {
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->readDatabase->adapter;
    $params = [
        'host'     => $config->readDatabase->host,
        'username' => $config->readDatabase->username,
        'password' => $config->readDatabase->password,
        'dbname'   => $config->readDatabase->dbname,
        'charset'  => $config->readDatabase->charset
    ];

    if ($config->readDatabase->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    // Set db logger
    $eventsManager = new \Phalcon\Events\Manager();
    $logger = new \Phalcon\Logger\Adapter\File($config->application->tmpDir . '/sql.log');
    $eventsManager->attach('db', function(\Phalcon\Events\Event $event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $vars = $connection->getSQLVariables();
            $logger->log($connection->getSQLStatement().' '.join(', ', $vars ?: []));
        }
    });
    $connection->setEventsManager($eventsManager);

    return $connection;
});

/**
 * Cache modelsMetadata with APC
 */
$di->setShared('modelsMetadata', function() {
    // Instantiate a metadata adapter, cache for 7 days
    $metadata = new \Phalcon\Mvc\Model\MetaData\Memcache([
        'lifetime' => 7*86400,
        'prefix'   => 'ndt_tbl_',
    ]);

    return $metadata;
});

/**
 * Setting up write database connection
 */
if (!empty($config->writeDatabase)) {
    $di->setShared('writeDb', function() use ($config) {
        $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->writeDatabase->adapter;
        $params = [
            'host'     => $config->writeDatabase->host,
            'username' => $config->writeDatabase->username,
            'password' => $config->writeDatabase->password,
            'dbname'   => $config->writeDatabase->dbname,
            'charset'  => $config->writeDatabase->charset
        ];

        if ($config->writeDatabase->adapter == 'Postgresql') {
            unset($params['charset']);
        }

        $connection = new $class($params);

        return $connection;
    });
}

/**
 * Memcached service hook up
 */
$di->setShared('memcached', function() use ($config) {
    // Set lifetime for memcached data
    $memcache = new \Phalcon\Cache\Frontend\Data([
        'lifetime' => $config->memcached->lifetime
    ]);

    // Create the Cache setting memcached connection options
    $cache = new \Phalcon\Cache\Backend\Memcache($memcache, [
        'host' => $config->memcached->host,
        'port' => $config->memcached->port,
        'persistent' => false
    ]);

    return $cache;
});

/**
 * Setting up dispatcher use a default namespace
 */
$di->setShared('dispatcher', function() use ($config) {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setDefaultNamespace('Ixosoftware\Cms\Controllers');

    // Set error handler
    $eventsManager = new \Phalcon\Events\Manager();
    $eventsManager->attach('dispatch:beforeException', function(\Phalcon\Events\Event $event, \Phalcon\Dispatcher $dispatcher, Exception $e) {

        if (APPLICATION_ENV == PRODUCTION) {
            switch ($e->getCode()) {
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    $dispatcher->forward([
                        'controller' => 'error-handler',
                        'action' => 'notFound'
                    ]);
                    break;

                default:
                    $dispatcher->forward([
                        'controller' => 'error-handler',
                        'action' => 'index'
                    ]);
                    break;
            }
            return false;
        }
    });

    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});