<?php

$router = $di->getRouter();

// This does NOT allow to have trailing slash at the end of each route
$router->removeExtraSlashes(true);

// Default, home page
$router->addGet('/', [
    'controller' => 'index',
    'action'     => 'index'
]);

// Route to category
$router->addGet('/c/{slug}/:params', [
    'controller' => 'category',
    'action'     => 'index',
    'params'     => 1
]);

$router->addGet('/c/{slug}/page/{page}', [
    'controller' => 'category',
    'action'     => 'index'
]);

// Route to article view
$router->addGet('/{slug}-a{articleId}.(html|amp)', [
    'controller' => 'article',
    'action'     => 'index',
    'type'       => 3
]);

// Route to search
$router->addGet('/search', [
    'controller' => 'search',
    'action'     => 'index'
]);

// Route to tag
$router->addGet('/tag/{slug}', [
    'controller' => 'tag',
    'action'     => 'index'
]);

$router->addGet('/tag/{slug}/page/{page}', [
    'controller' => 'tag',
    'action'     => 'index'
]);

// Route to event
$router->addGet('/ev{eventId}/{slug}.html', [
    'controller' => 'event',
    'action'     => 'index'
]);

$router->addGet('/ev{eventId}/{slug}/page/{page}.html', [
    'controller' => 'event',
    'action'     => 'index'
]);

// Comment routes
$router->addGet('/article/{articleId}/comments', [
    'controller' => 'comment',
    'action'     => 'articleComments'
]);

$router->addGet('/reCaptcha', [
    'controller' => 'comment',
    'action'     => 'captcha'
]);

$router->addPost('/article/{articleId}/comments', [
    'controller' => 'article',
    'action'     => 'postComment'
]);

$router->addPost('/article/{articleId}/comments/like/{commentId}', [
    'controller' => 'article',
    'action'     => 'likeComment'
]);

// Not found handler
$router->notFound([
    'controller' => 'error-handler',
    'action'     => 'index'
]);

// TODO: Remove this after done
$router->addGet('/modelMetaData', [
    'controller' => 'index',
    'action'     => 'metaData'
]);

$router->handle();

