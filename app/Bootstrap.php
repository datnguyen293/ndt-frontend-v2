<?php
namespace Ixosoftware\Cms;
use Phalcon\Di\FactoryDefault;

class Bootstrap
{
    public function run()
    {
        try {
            /**
             * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
             */
            $di = new FactoryDefault();

            /**
             * Load routes
             */
            include APPLICATION_PATH . '/config/router.php';

            /**
             * Load the configuration file based on environment variable
             */
            if (APPLICATION_ENV == DEVELOPMENT) {
                $config = include APPLICATION_PATH . '/config/environment/development.php';
            }
            else {
                $config = include APPLICATION_PATH . '/config/environment/production.php';
            }
            $di->setShared('config', function() use ($config) {
                return $config;
            });

            /**
             * Load auto-loader
             */
            include APPLICATION_PATH . '/config/loader.php';

            /**
             * Load registered services
             */
            include APPLICATION_PATH . '/config/services.php';

            /**
             * Start application to handle request
             */
            $app = new \Phalcon\Mvc\Application($di);

            /**
             * Caching
             */
            $fileName = null;

            $controllerName = $app->router->getControllerName();
            $actionName = $app->router->getActionName();

            if ($actionName == 'index') {
                switch ($controllerName) {
                    case 'article':
                        $fileName = $app->router->getRewriteUri();
                        break;

                    case 'index':
                        $fileName = 'index.html';
                        break;

                    case 'category':
                        $fileName = trim($app->router->getRewriteUri(), '/ ') . '.html';
                        break;

                    default:
                        break;
                }
            }

            $content = $app->handle()->getContent();

            if (!empty($di->getShared('config')->output->minifyHtml)) {
                $content = $this->minifyHtml($content);
            }

            if (!empty($fileName) && !empty($di->getShared('config')->output->cacheHtml)) {
                $cacheFilePath = ROOT . '/public/cache/' . $_SERVER['SERVER_NAME'] . '/' . ltrim($fileName, '/ ');
                $folderPath = dirname($cacheFilePath);
                if (!file_exists($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                $fh = fopen($cacheFilePath, 'w') or $fh = null;
                if ($fh) {
                    fwrite($fh, $content);
                    fclose($fh);
                }
            }

            echo $content;
        }
        catch (\Exception $e) {
            echo $e->getMessage() . '<br/>';
            echo '<pre>' . $e->getTraceAsString() . '</pre>';
        }
    }

    private function minifyHtml($html)
    {
        $search = array(
            /*'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space*/
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        );

        $replace = array(
            /*'>',
            '<',*/
            '\\1',
            ''
        );

        $buffer = preg_replace($search, $replace, $html);

        return $buffer;
    }
}
