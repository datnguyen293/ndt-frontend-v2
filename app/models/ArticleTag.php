<?php
namespace Ixosoftware\Cms\Models;

class ArticleTag extends BaseModel
{
    public $id;
    public $articleId;
    public $tagId;

    public function getSource()
    {
        return 'Article_Tags';
    }

    public function columnMap()
    {
        return [
            'ID' => 'id',
            'ArticleID' => 'articleId',
            'TagID'     => 'tagId'
        ];
    }
}