<?php
namespace Ixosoftware\Cms\Models;

class Page extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $title;
    public $description;
    public $metaDescription;
    public $metaKeyword;
    public $metaTitle;
    public $metaSlug;
    public $content;
    public $backgroundColor;
    public $createdById;
    public $modifiedById;
    public $backgroundImageId;
    public $esIndex;
    public $isEmagazine;
    public $status;

    public function getSource()
    {
        return 'Page';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Title'         => 'title',
            'Description'   => 'description',
            'MetaDescription' => 'metaDescription',
            'MetaKeyword'   => 'metaKeyword',
            'MetaTitle'     => 'metaTitle',
            'MetaSlug'      => 'metaSlug',
            'Content'       => 'content',
            'BackgroundColor' => 'backgroundColor',
            'CreatedByID'   => 'createdById',
            'ModifiedByID'  => 'modifiedById',
            'BackgroundImageID' => 'backgroundImageId',
            'ESIndex'       => 'esIndex',
            'IsEmagazine'   => 'isEmagazine',
            'Status'        => 'status'
        ];
    }
}