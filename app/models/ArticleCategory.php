<?php
namespace Ixosoftware\Cms\Models;

class ArticleCategory extends BaseModel
{
    public $id;
    public $articleId;
    public $categoryId;

    public function getSource()
    {
        return 'Article_Categories';
    }

    public function columnMap()
    {
        return [
            'ID' => 'id',
            'ArticleID' => 'articleId',
            'CategoryID' => 'categoryId'
        ];
    }
}