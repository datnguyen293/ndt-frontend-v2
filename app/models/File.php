<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class File extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $name;
    public $title;
    public $fileName;
    public $content;
    public $showInSearch;
    public $parentId;
    public $ownerId;

    public function getSource()
    {
        return 'File';
    }

    public function columnMap()
    {
        return [
            'ID'        => 'id',
            'ClassName' => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Name'          => 'name',
            'Title'         => 'title',
            'Filename'      => 'fileName',
            'Content'       => 'content',
            'ShowInSearch'  => 'showInSearch',
            'ParentID'      => 'parentId',
            'OwnerID'       => 'ownerId'
        ];
    }

    public function metaData()
    {
        return [
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Name', 'Title', 'Filename', 'Content', 'ShowInSearch', 'ParentID', 'OwnerID',
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}