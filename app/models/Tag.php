<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Tag extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $title;
    public $description;
    public $metaDescription;
    public $metaKeyword;
    public $metaTitle;
    public $metaSlug;
    public $content;
    public $backgroundColor;
    public $backgroundImageId;
    public $status;
    public $createdById;
    public $anchorText;
    public $anchorTextActive;
    public $esIndex;
    public $moved;

    public function getSource()
    {
        return 'Tag';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Title'         => 'title',
            'Description'   => 'description',
            'MetaDescription' => 'metaDescription',
            'MetaKeyword'   => 'metaKeyword',
            'MetaTitle'     => 'metaTitle',
            'MetaSlug'      => 'metaSlug',
            'Content'       => 'content',
            'BackgroundColor' => 'backgroundColor',
            'BackgroundImageID' => 'backgroundImageId',
            'Status'        => 'status',
            'CreatedByID'   => 'createdById',
            'AnchorText'    => 'anchorText',
            'AnchorTextActive'  => 'anchorTextActive',
            'ESIndex'       => 'esIndex',
            'Moved'         => 'moved'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Title', 'Description', 'MetaDescription', 'MetaKeyword', 'MetaTitle', 'MetaSlug', 'Content', 'BackgroundColor', 'BackgroundImageID', 'Status', 'CreatedByID', 'AnchorText', 'AnchorTextActive', 'ESIndex', 'Moved',
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}