<?php
namespace Ixosoftware\Cms\Models;

class Poll extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $name;
    public $title;
    public $content;
    public $type;
    public $status;
    public $createdById;
    public $modifiedById;

    public function getSource()
    {
        return 'Poll';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Name'          => 'name',
            'Title'         => 'title',
            'Content'       => 'content',
            'Type'          => 'type',
            'Status'        => 'status',
            'CreatedByID'   => 'createdById',
            'ModifiedByID'  => 'modifiedById'
        ];
    }
}