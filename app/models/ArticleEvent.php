<?php
namespace Ixosoftware\Cms\Models;

class ArticleEvent extends BaseModel
{
    public $id;
    public $articleId;
    public $eventId;

    public function getSource()
    {
        return 'Article_Events';
    }

    public function columnMap()
    {
        return [
            'ID'    => 'id',
            'ArticleID' => 'articleId',
            'EventID'   => 'eventId'
        ];
    }
}