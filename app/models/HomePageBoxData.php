<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class HomePageBoxData extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $json;
    public $boxId;
    public $boxType;
    public $boxName;
    public $status;
    public $categoryId;
    public $homePageId;
    public $createdById;

    public function getSource()
    {
        return 'HomePageBoxData';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Json'          => 'json',
            'BoxID'         => 'boxId',
            'BoxType'       => 'boxType',
            'BoxName'       => 'boxName',
            'Status'        => 'status',
            'CategoryID'    => 'categoryId',
            'HomePageID'    => 'homePageId',
            'CreatedByID'   => 'createdById'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Json', 'BoxID', 'BoxType', 'BoxName', 'Status', 'CategoryID', 'HomePageID', 'CreatedByID'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}