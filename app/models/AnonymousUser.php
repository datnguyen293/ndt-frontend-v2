<?php
namespace Ixosoftware\Cms\Models;

class AnonymousUser extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $fullName;
    public $email;
    public $phone;
    public $ip;
    public $userAgent;
    public $referrer;

    public function getSource()
    {
        return 'AnonymousUser';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'FullName'      => 'fullName',
            'Email'         => 'email',
            'Phone'         => 'phone',
            'IP'            => 'phone',
            'User-Agent'    => 'userAgent',
            'Referrer'       => 'referrer'
        ];
    }

    /*public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'FullName', 'Email', 'Phone', 'IP', 'User-Agent', 'Referrer',
            ],

            // Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            // The identity column, use boolean false if the model doesn't have
            // an identity column
            MetaData::MODELS_IDENTITY_COLUMN => 'ID',

            // Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => [
            ],

            // Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => [],

            // Every column and their data types
            MetaData::MODELS_DATA_TYPES => [
                'ID' => Column::TYPE_INTEGER,
                'ClassName' => Column::TYPE_VARCHAR,
                'LastEdited' => Column::TYPE_DATETIME,
                'Created' => Column::TYPE_DATETIME,
                'FullName' => Column::TYPE_VARCHAR,
                'Email' => Column::TYPE_VARCHAR,
                'Phone' => Column::TYPE_VARCHAR,
                'IP' => Column::TYPE_VARCHAR,
                'User-Agent' => Column::TYPE_VARCHAR,
                'Referrer' => Column::TYPE_VARCHAR,
            ],

            // The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => [

            ],

            // How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => [
            ],

            // Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => [
            ],

            // Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => [
            ],

            // Default values for columns
            MetaData::MODELS_DEFAULT_VALUES => [
                'ClassName' => 'AnonymousUser',
                'LastEdited' => date('Y-m-d H:i:s')
            ],

            // Fields that allow empty strings
            MetaData::MODELS_EMPTY_STRING_VALUES => [
                'Email' => true,
                'FullName' => true,
                'Phone' => true,
                'IP' => true,
                'User-Agent' => true,
                'Referrer' => true,
            ],
        ];
    }*/
}