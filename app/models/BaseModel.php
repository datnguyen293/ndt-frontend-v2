<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model;

class BaseModel extends Model
{
    public function initialize()
    {
        $this->setReadConnectionService('db');
        $this->setWriteConnectionService('writeDb');
    }

    public function getMessages($filter = null)
    {
        $messages = array();

        foreach (parent::getMessages() as $message) {
            if (!isset($messages[$message->getField()])) {
                $messages[$message->getField()] = array();
            }
            array_push($messages[$message->getField()], array(
                'message' => $message->getMessage(),
                'type'	  => $message->getType()
            ));
        }

        return $messages;
    }
}