<?php
namespace Ixosoftware\Cms\Models;

class Journalist extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $fullName;
    public $penName;
    public $phone;
    public $cmnd;
    public $email;
    public $facebook;
    public $googlePlus;
    public $birthday;
    public $status;
    public $avatarId;

    public function getSource()
    {
        return 'Journalist';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'FullName'      => 'fullName',
            'PenName'       => 'penName',
            'Phone'         => 'phone',
            'CMND'          => 'cmnd',
            'Email'         => 'email',
            'Facebook'      => 'facebook',
            'GPlus'         => 'googlePlus',
            'BirthDay'      => 'birthday',
            'Status'        => 'status',
            'AvatarID'      => 'avatarId'
        ];
    }
}