<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Menu extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $name;
    public $content;
    public $position;
    public $status;
    public $createdById;
    public $modifiedById;

    public function getSource()
    {
        return 'Menu';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Name'          => 'name',
            'Content'       => 'content',
            'Position'      => 'position',
            'Status'        => 'status',
            'CreatedByID'   => 'createdById',
            'ModifiedByID'  => 'modifiedById'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Name', 'Content', 'Position', 'Status', 'CreatedByID', 'ModifiedByID'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}