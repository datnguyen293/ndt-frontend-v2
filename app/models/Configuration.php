<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Configuration extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $description;
    public $jsonConfig;
    public $configType;
    public $status;
    
    public function getSource()
    {
        return 'Configuration';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Description'   => 'description',
            'JsonConfig'    => 'jsonConfig',
            'ConfigType'    => 'configType',
            'Status'        => 'status'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Description', 'JsonConfig', 'ConfigType', 'Status'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}