<?php
namespace Ixosoftware\Cms\Models;

class MemberPassword extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $password;
    public $salt;
    public $passwordEncryption;
    public $memberId;

    public function getSource()
    {
        return 'MemberPassword';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Password'      => 'password',
            'Salt'          => 'salt',
            'PasswordEncryption' => 'passwordEncryption',
            'MemberID'      => 'memberId'
        ];
    }
}