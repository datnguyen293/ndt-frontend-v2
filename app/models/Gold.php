<?php
namespace Ixosoftware\Cms\Models;

class Gold extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $content;
    public $updatedTime;

    public function getSource()
    {
        return 'Gold';
    }

    public function columnMap()
    {
        return [
            'ID' => 'id',
            'ClassName' => 'className',
            'LastEdited' => 'lastEdited',
            'Created' => 'created',
            'Content' => 'content',
            'UpdateTime' => 'updatedTime'
        ];
    }
}