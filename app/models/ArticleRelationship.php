<?php
namespace Ixosoftware\Cms\Models;

class ArticleRelationship extends BaseModel
{
    public $id;
    public $articleId;
    public $childId;

    public function getSource()
    {
        return 'Article_Relationship';
    }

    public function columnMap()
    {
        return [
            'ID' => 'id',
            'ArticleID' => 'articleId',
            'ChildID'   => 'childId'
        ];
    }
}