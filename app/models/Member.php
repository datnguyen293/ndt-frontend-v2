<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Member extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $firstName;
    public $surName;
    public $email;
    public $tempIdHash;
    public $tempIdExpired;
    public $password;
    public $rememberLoginToken;
    public $numVisit;
    public $lastVisited;
    public $autoLoginHash;
    public $autoLoginExpired;
    public $passwordEncryption;
    public $salt;
    public $passwordExpiry;
    public $lockedOutUntil;
    public $locale;
    public $failedLoginCount;
    public $dateFormat;
    public $timeFormat;
    public $username;
    public $penName;
    public $sex;
    public $phone;
    public $avatar;
    public $deleted;
    public $allowActivity;
    public $departmentId;
    public $avatarId;
    public $verifyLogin;
    public $wfPosition;
    public $avatarWaitReviewId;
    public $previewCookie;

    public function getSource()
    {
        return 'Member';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'FirstName'     => 'firstName',
            'Surname'       => 'surName',
            'Email'         => 'email',
            'TempIDHash'    => 'tempIdHash',
            'TempIDExpired' => 'tempIdExpired',
            'Password'      =>'password',
            'RememberLoginToken' => 'rememberLoginToken',
            'NumVisit'      => 'numVisit',
            'LastVisited'   => 'lastVisited',
            'AutoLoginHash' => 'autoLoginHash',
            'AutoLoginExpired' => 'autoLoginExpired',
            'PasswordEncryption' => 'passwordEncryption',
            'Salt'          => 'salt',
            'PasswordExpiry'=> 'passwordExpiry',
            'LockedOutUntil'=> 'lockedOutUntil',
            'Locale'        => 'locale',
            'FailedLoginCount' => 'failedLoginCount',
            'DateFormat'    => 'dateFormat',
            'TimeFormat'    => 'timeFormat',
            'Username'      => 'username',
            'PenName'       => 'penName',
            'Sex'           => 'sex',
            'Phone'         => 'phone',
            'Avatar'        => 'avatar',
            'Deleted'       => 'deleted',
            'AllowActivity' => 'allowActivity',
            'DepartmentID'  => 'departmentId',
            'AvatarID'      => 'avatarId',
            'VerifyLogin'   => 'verifyLogin',
            'WFPosition'    => 'wfPosition',
            'AvatarWaitReviewID' => 'avatarWaitReviewId',
            'PreviewCookie' => 'previewCookie'
        ];
    }

    public function metaData()
    {
        return [
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'FirstName', 'Surname', 'Email', 'TempIDHash', 'TempIDExpired', 'Password', 'RememberLoginToken', 'NumVisit', 'LastVisited', 'AutoLoginHash', 'AutoLoginExpired', 'PasswordEncryption', 'Salt', 'PasswordExpiry', 'LockedOutUntil', 'Locale', 'FailedLoginCount', 'DateFormat', 'TimeFormat', 'Username', 'PenName', 'Sex', 'Phone', 'Avatar', 'Deleted', 'AllowActivity', 'DepartmentID', 'AvatarID', 'VerifyLogin', 'WFPosition', 'AvatarWaitReviewID', 'PreviewCookie',
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}