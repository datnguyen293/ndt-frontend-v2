<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Pin extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $ordering;
    public $hits;
    public $articleId;
    public $categoryId;
    public $createdById;
    public $modifiedById;

    public function getSource()
    {
        return 'Pin';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Ordering'      => 'ordering',
            'Hits'          => 'hits',
            'ArticleID'     => 'articleId',
            'CategoryID'    => 'categoryId',
            'CreatedByID'   => 'createdById',
            'ModifiedByID'  => 'modifiedById'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Ordering', 'Hits', 'ArticleID', 'CategoryID', 'CreatedByID', 'ModifiedByID'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}