<?php
namespace Ixosoftware\Cms\Models;

use Ixosoftware\Cms\Helpers\Scaffolding;
use Phalcon\Mvc\Model\MetaData;

class Article extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $reference;
    public $contentType;
    public $note;
    public $reason;
    public $jsonContent;
    public $publishTime;
    public $allowComment;
    public $cmtnd;
    public $commentCount;
    public $stage;
    public $status;
    public $royaltyStage;
    public $createdById;
    public $currentStep;
    public $modifiedById;
    public $beforeStepUserId;
    public $currentStepUserId;
    public $involved;
    public $version;
    public $title;
    public $description;
    public $metaDescription;
    public $metaKeyword;
    public $metaTitle;
    public $metaSlug;
    public $content;
    public $imageId;
    public $primaryCategoryId;
    public $articleTypeId;
    public $journalistId;
    public $crawlerId;
    public $sapo;
    public $sourceId;
    public $showComment;
    public $showJournalist;
    public $expertise;
    public $viewCount;
    public $autoSave;
    public $showInNew;
    public $backgroundColor;
    public $backgroundImageId;
    public $sanitized;
    public $categoryIds;
    public $tagIds;
    public $eventIds;
    public $pollId;
    public $setRoyalty;
    public $lockUntil;
    public $expertiseById;
    public $editingById;
    public $setRoyaltyById;
    public $pageView;
    public $visit;
    public $timeOnPage;
    public $ga;
    public $salary;
    public $createdByPosition;
    public $liveUpdateTime;
    public $esIndex;

    public function getSimpleDisplayData()
    {
        return [
            'id' => $this->id,
            'metaSlug' => $this->metaSlug,
            'slug' => Scaffolding::getArticleSlug($this->metaSlug, $this->id),
            'title' => $this->title,
            'metaTitle' => $this->metaTitle
        ];
    }

    /**
     * Set table name for this table
     */
    public function getSource()
    {
        return 'Article';
    }

    /**
     * Mapping columns to model properties
     * This function returns an array where 'keys' are the real names in the table
     * and 'values' are their names in the application
     */
    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Reference'     => 'reference',
            'ContentType'   => 'contentType',
            'Note'          => 'note',
            'Reason'        => 'reason',
            'JsonContent'   => 'jsonContent',
            'PublishTime'   => 'publishTime',
            'AllowComment'  => 'allowComment',
            'CMTND'         => 'cmtnd',
            'CommentCount'  => 'commentCount',
            'Stage'         => 'stage',
            'Status'        => 'status',
            'RoyaltyStage'  => 'royaltyStage',
            'CreatedByID'   => 'createdById',
            'CurrentStep'   => 'currentStep',
            'ModifiedByID'  => 'modifiedById',
            'BeforeStepUserID'  => 'beforeStepUserId',
            'CurrentStepUserID' => 'currentStepUserId',
            'Involved'      => 'involved',
            'Version'       => 'version',
            'Title'         => 'title',
            'Description'   => 'description',
            'MetaDescription' => 'metaDescription',
            'MetaKeyword'   => 'metaKeyword',
            'MetaTitle'     => 'metaTitle',
            'MetaSlug'      => 'metaSlug',
            'Content'       => 'content',
            'ImageID'       => 'imageId',
            'PrimaryCategoryID' => 'primaryCategoryId',
            'ArticleTypeID' => 'articleTypeId',
            'JournalistID'  => 'journalistId',
            'CrawlerID'     => 'crawlerId',
            'Sapo'          => 'sapo',
            'SourceID'      => 'sourceId',
            'ShowComment'   => 'showComment',
            'ShowJournalist'=> 'showJournalist',
            'Expertise'     => 'expertise',
            'ViewCount'     => 'viewCount',
            'Autosave'      => 'autoSave',
            'ShowInNew'     => 'showInNew',
            'BackgroundColor'   => 'backgroundColor',
            'BackgroundImageID' => 'backgroundImageId',
            'Sanitized'     => 'sanitized',
            'CategoryIDs'   => 'categoryIds',
            'TagIDs'        => 'tagIds',
            'EventIDs'      => 'eventIds',
            'PollID'        => 'pollId',
            'SetRoyalty'    => 'setRoyalty',
            'LockUntil'     => 'lockUntil',
            'ExpertiseByID' => 'expertiseById',
            'EdittingByID'  => 'editingById',
            'SetRoyaltyByID'=> 'setRoyaltyById',
            'Pageview'      => 'pageView',
            'Visit'         => 'visit',
            'TimeOnPage'    => 'timeOnPage',
            'GA'            => 'ga',
            'Salary'        => 'salary',
            'CreatedByPosition' => 'createdByPosition',
            'LiveUpdateTime'    => 'liveUpdateTime',
            'ESIndex'       => 'esIndex',
        ];
    }

    public function metaData()
    {
        return [
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Reference', 'ContentType', 'Note', 'Reason', 'JsonContent', 'PublishTime', 'AllowComment', 'CMTND', 'CommentCount', 'Stage', 'Status', 'RoyaltyStage', 'CreatedByID', 'CurrentStep', 'ModifiedByID', 'BeforeStepUserID', 'CurrentStepUserID', 'Involved', 'Version', 'Title', 'Description', 'MetaDescription', 'MetaKeyword', 'MetaTitle', 'MetaSlug', 'Content', 'ImageID', 'PrimaryCategoryID', 'ArticleTypeID', 'JournalistID', 'CrawlerID', 'Sapo', 'SourceID', 'ShowComment', 'ShowJournalist', 'Expertise', 'ViewCount', 'Autosave', 'ShowInNew', 'BackgroundColor', 'BackgroundImageID', 'Sanitized', 'CategoryIDs', 'TagIDs', 'EventIDs', 'PollID', 'SetRoyalty', 'LockUntil', 'ExpertiseByID', 'EdittingByID', 'SetRoyaltyByID', 'Pageview', 'Visit', 'TimeOnPage', 'GA', 'Salary', 'CreatedByPosition', 'LiveUpdateTime', 'ESIndex',
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}