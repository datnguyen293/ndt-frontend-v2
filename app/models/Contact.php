<?php
namespace Ixosoftware\Cms\Models;

class Contact extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $name;
    public $email;
    public $facebook;
    public $message;
    public $selected;
    public $title;

    public function getSource()
    {
        return 'Contact';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Name'          => 'name',
            'Email'         => 'email',
            'Facebook'      => 'facebook',
            'Message'       => 'message',
            'Selected'      => 'selected',
            'Title'         => 'title'
        ];
    }
}