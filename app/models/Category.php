<?php
namespace Ixosoftware\Cms\Models;

use Ixosoftware\Cms\Helpers\Scaffolding;
use Phalcon\Mvc\Model\MetaData;

class Category extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $ordering;
    public $description;
    public $status;
    public $title;
    public $metaDescription;
    public $metaKeyword;
    public $metaTitle;
    public $metaSlug;
    public $content;
    public $parentId;
    public $imageId;
    public $backgroundColor;
    public $pinedEventId;
    public $backgroundImageId;
    public $priority;
    public $adsJson;
    public $pinedPollId;
    public $gaTrackingCode;
    public $esIndex;

    public function getSimpleDisplayData()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'metaSlug' => Scaffolding::getCategorySlug($this->metaSlug)
        ];
    }

    public function getSource()
    {
        return 'Category';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Ordering'      => 'ordering',
            'Description'   => 'description',
            'Status'        => 'status',
            'Title'         => 'title',
            'MetaDescription'   => 'metaDescription',
            'MetaKeyword'   => 'metaKeyword',
            'MetaTitle'     => 'metaTitle',
            'MetaSlug'      => 'metaSlug',
            'Content'       => 'content',
            'ParentID'      => 'parentId',
            'ImageID'       => 'imageId',
            'BackgroundColor'   => 'backgroundColor',
            'PinedEventID'  => 'pinedEventId',
            'BackgroundImageID' => 'backgroundImageId',
            'Priority'      => 'priority',
            'AdsJson'       => 'adsJson',
            'PinedPollID'   => 'pinedPollId',
            'GATrackingCode'    => 'gaTrackingCode',
            'ESIndex'       => 'esIndex'
        ];
    }

    public function metaData()
    {
        return [
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Ordering', 'Description', 'Status', 'Title', 'MetaDescription', 'MetaKeyword', 'MetaTitle', 'MetaSlug', 'Content', 'ParentID', 'ImageID', 'BackgroundColor', 'PinedEventID', 'BackgroundImageID', 'Priority', 'AdsJson', 'PinedPollID', 'GATrackingCode', 'ESIndex',
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}