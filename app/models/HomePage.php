<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class HomePage extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $title;
    public $content;
    public $metaTitle;
    public $metaDescription;
    public $metaKeywords;
    public $active;
    public $deleted;
    public $createdById;
    public $modifiedById;

    public function getSource()
    {
        return 'HomePage';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Title'         => 'title',
            'Content'       => 'content',
            'MetaTitle'     => 'metaTitle',
            'MetaDescription' => 'metaDescription',
            'MetaKeywords'  => 'metaKeywords',
            'Active'        => 'active',
            'Deleted'       => 'deleted',
            'CreatedByID'   => 'createdById',
            'ModifiedByID'  => 'modifiedById'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Title', 'Content', 'MetaTitle', 'MetaDescription', 'MetaKeywords', 'Active', 'Deleted', 'CreatedByID', 'ModifiedByID'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}