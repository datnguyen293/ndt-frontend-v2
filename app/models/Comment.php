<?php
namespace Ixosoftware\Cms\Models;

class Comment extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $content;
    public $status;
    public $articleId;
    public $parentId;
    public $userId;
    public $likesId;
    public $anonymousUserId;
    public $modifiedById;
    public $ip;
    public $userAgent;
    public $articleCategoryIds;
    public $likeCount;

    public function toJson()
    {
        return [
            'id' => (int)$this->id,
            'created' => strtotime($this->created),
            'lastEdited' => strtotime($this->lastEdited),
            'content' => $this->content,
            'status' => $this->status,
            'articleId' => $this->articleId,
            'parentId' => $this->parentId,
            'userId' => $this->userId,
            'anonymousUserId' => $this->anonymousUserId,
            'likeCount' => $this->likeCount
        ];
    }

    public function getSource()
    {
        return 'Comment';
    }

    public function columnMap()
    {
        return [
            'ID'        => 'id',
            'ClassName' => 'className',
            'LastEdited'=> 'lastEdited',
            'Created'   => 'created',
            'Content'   => 'content',
            'Status'    => 'status',
            'ArticleID' => 'articleId',
            'ParentID'  => 'parentId',
            'UserID'    => 'userId',
            'LikesID'   => 'likesId',
            'AnonymousUserID'   => 'anonymousUserId',
            'ModifiedByID'      => 'modifiedById',
            'IP'        => 'ip',
            'Useragent' => 'userAgent',
            'ArticleCategoryIDs'=> 'articleCategoryIds',
            'LikeCount' => 'likeCount'
        ];
    }
}