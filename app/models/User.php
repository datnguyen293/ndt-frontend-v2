<?php
namespace Ixosoftware\Cms\Models;

class User extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $facebookId;
    public $facebookToken;
    public $googlePlusId;
    public $googlePlusToken;
    public $email;
    public $phone;
    public $username;
    public $displayName;
    public $address;
    public $cmnd;
    public $avatar;
    public $numVisit;
    public $password;
    public $passwordEncryption;
    public $salt;
    public $lockedOutUntil;
    public $failedLoginCount;
    public $status;

    public function getSource()
    {
        return 'User';
    }

    public function columnMap()
    {
        return [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'FacebookID'    => 'facebookId',
            'FacebookToken' => 'facebookToken',
            'GPlusID'       => 'googlePlusId',
            'GPlusToken'    => 'googlePlusToken',
            'Email'         => 'email',
            'Phone'         => 'phone',
            'Username'      => 'username',
            'DisplayName'   => 'displayName',
            'Address'       => 'address',
            'CMND'          => 'cmnd',
            'Avatar'        => 'avatar',
            'NumVisit'      => 'numVisit',
            'Password'      => 'password',
            'PasswordEncryption' => 'passwordEncryption',
            'Salt'          => 'salt',
            'LockedOutUntil' => 'lockedOutUntil',
            'FailedLoginCount'  => 'failedLoginCount',
            'Status'        => 'status'
        ];
    }
}