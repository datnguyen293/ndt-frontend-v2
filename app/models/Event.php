<?php
namespace Ixosoftware\Cms\Models;

use Phalcon\Mvc\Model\MetaData;

class Event extends BaseModel
{
    public $id;
    public $className;
    public $lastEdited;
    public $created;
    public $status;
    public $title;
    public $description;
    public $metaDescription;
    public $metaKeyword;
    public $metaTitle;
    public $metaSlug;
    public $content;
    public $categoryId;
    public $backgroundColor;
    public $imageId;
    public $backgroundImageId;
    public $anchorText;
    public $anchorTextActive;
    public $esIndex;

    public function getSource()
    {
        return 'Event';
    }

    public function columnMap()
    {
        return [
            'ID'        => 'id',
            'ClassName' => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'Status'        => 'status',
            'Title'         => 'title',
            'Description'   => 'description',
            'MetaDescription'   => 'metaDescription',
            'MetaKeyword'   => 'metaKeyword',
            'MetaTitle'     => 'metaTitle',
            'MetaSlug'      => 'metaSlug',
            'Content'       => 'content',
            'CategoryID'    => 'categoryId',
            'BackgroundColor' => 'backgroundColor',
            'ImageID'       => 'imageId',
            'BackgroundImageID' => 'backgroundImageId',
            'AnchorText'    => 'anchorText',
            'AnchorTextActive' => 'anchorTextActive',
            'ESIndex'       => 'esIndex'
        ];
    }

    public function metaData()
    {
        return [
            // Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => [
                'ID', 'ClassName', 'LastEdited', 'Created', 'Status', 'Title', 'Description', 'MetaDescription', 'MetaKeyword', 'MetaTitle', 'MetaSlug', 'Content', 'CategoryID', 'BackgroundColor', 'ImageID', 'BackgroundImageID', 'AnchorText', 'AnchorTextActive', 'ESIndex'
            ],

            MetaData::MODELS_PRIMARY_KEY => ['ID'],

            MetaData::MODELS_IDENTITY_COLUMN => ['ID']
        ];
    }
}