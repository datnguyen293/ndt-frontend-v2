<?php
namespace Ixosoftware\Cms\Helpers;

class Date
{
    private static $nice_format = 'd/m/Y g:ia';
    private static $nice_24h_format = 'd/m/Y H:i';
    private static $date_only_format = 'd/m/Y';
    private static $time_only_format = 'g:ia';
    private static $time_24h_only_format = 'H:i';

    public function nice($date)
    {
        $dateTime = new \DateTime($date);
        return $dateTime->format(Date::$nice_format);
    }

    public function nice24h($date)
    {
        $dateTime = new \DateTime($date);
        return $dateTime->format(Date::$nice_24h_format);
    }

    public function dateOnly($date)
    {
        $dateTime = new \DateTime($date);
        return $dateTime->format(Date::$date_only_format);
    }

    public function timeOnly($date)
    {
        $dateTime = new \DateTime($date);
        return $dateTime->format(Date::$time_only_format);
    }

    public function time24hOnly($date)
    {
        $dateTime = new \DateTime($date);
        return $dateTime->format(Date::$time_24h_only_format);
    }
}