<?php
namespace Ixosoftware\Cms\Helpers;

class Category
{
    private $di;
    private $allCategories;
    
    public function __construct()
    {
        $this->di = \Phalcon\Di::getDefault();
        $this->allCategories = Memcached::getAllCategories();
    }

    public function getAllCategories()
    {
        return $this->allCategories;
    }

    public function getCategoryBySlug($slug)
    {
        foreach ($this->allCategories as $categoryId => $category) {
            if ($category->metaSlug == $slug) {
                return $category;
            }
        }
        return null;
    }

    public function getCategoryById($categoryId) {
        if (isset($this->allCategories[$categoryId])) {
            return $this->allCategories[$categoryId];
        }
        return null;
    }

    /**
     * Get root category of a category
     * Returns itself if no parent
     *
     */
    public function getRootCategory($category)
    {
        if (empty($category)) return null;

        if (empty($category->parentId)) return $category;

        while (!empty($category->parentId) && isset($this->allCategories[$category->parentId])) {
            $category = $this->allCategories[$category->parentId];
        }

        return $category;
    }

    /**
     * Generate list of subcategories of a category
     *
     * @return array - List of subcategories IDs
     */
    public function getSubcategoryIds($category)
    {
        $categoryIds = [];

        if (empty($category)) {
            return $categoryIds;
        }

        foreach ($this->allCategories as $categoryId => $cat) {
            if ($cat->parentId == $category->id) {
                $categoryIds[] = $cat->id;
            }
        }

        return $categoryIds;
    }

    /**
     * Generate sub-navigation menu
     *  - If category has parent, add parent category to the menu list, and also its siblings
     *  - Otherwise, add this category and its own children to the menu list
     *
     * @return array - List of category ids in order from parent to children
     */
    public function generateSubNav($category, &$navCategoryIds = [])
    {
        $subNav = [];

        if (!empty($category)) {
            $parentCategory = $this->getRootCategory($category);
            $subNav[] = $this->navCategoryData($parentCategory);
            $navCategoryIds[] = $parentCategory->id;

            $subCategories = [];
            foreach ($this->allCategories as $cat) {
                if ($cat->parentId == $parentCategory->id) {
                    $subCategories[] = $this->navCategoryData($cat);
                    $navCategoryIds[] = $cat->id;
                }
            }

            // Sort subcategories by their ordering field
            usort($subCategories, function($item1, $item2) {
                if ($item1['ordering'] < $item2['ordering']) {
                    return -1;
                }
                if ($item1['ordering'] > $item2['ordering']) {
                    return 1;
                }
                return 0;
            });

            $subNav = array_merge($subNav, $subCategories);
        }

        return $subNav;
    }

    public function getParentCategories($category)
    {
        $categories = [];

        if (empty($category) || empty($category->parentId)) return $categories;

        while (!empty($category->parentId) && isset($this->allCategories[$category->parentId])) {
            $category = $this->allCategories[$category->parentId];
            array_unshift($categories, $category);
        }

        return $categories;
    }

    public function navCategoryData($category)
    {
        if (empty($category)) {
            return [];
        }
        return [
            'id' => $category->id,
            'parentId' => $category->parentId,
            'title' => $category->title,
            'metaSlug' => sprintf('/c/%s', $category->metaSlug),
            'ordering' => $category->ordering
        ];
    }

    /**
     * Get most read articles of categories
     * Rule: Lọc theo tin đọc nhiều nhất trong vòng 24h. Lấy 7h sáng làm mốc.
     *
     * @return array - List of articles
     */
    public function getMostReadArticles($mainCategoryId, $categoryIds = [], $limit = 10)
    {
        $memcached = $this->di->getShared('memcached');

        $cachedKey = MOST_READ_ARTICLES_CATEGORY_PREFIX . $mainCategoryId;
        $articles = $memcached->get($cachedKey);

        if (!empty($articles)) {
            return $articles;
        }

        //Get date in 24h ago
        $date = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime($date . '-1 days'));
        $dateFrom = date('Y-m-d', strtotime($yesterday)) . " 07:00:00";
        $dateTo = date('Y-m-d', strtotime($date)) . " 07:00:00";
        $dateVFrom = date('Y-m-d', strtotime($dateFrom)) . " 00:00:00";
        $dateVTo = date('Y-m-d', strtotime($dateFrom)) . " 23:59:59";

        // Crazy 'viu' name because 'view' is one of MySQL keyword
        $sql = 'SELECT art.ID as id, art.Title as title, art.PublishTime as publishTime, art.MetaSlug as metaSlug, art.ImageID as imageId, art.MetaTitle as metaTitle, art.LastEdited as lastEdited, art.ContentType as contentType, art.AllowComment as allowComment, art.ShowComment as showComment, art.CommentCount as commentCount, art.Sapo as sapo, viu.Hit as hit ' .
                'FROM Article AS art ' .
                'INNER JOIN ArticleViewInDay viu ON viu.ArticleID = art.ID ' .
                        'AND viu.`Day` >= ' . strtotime($dateVFrom) . ' ' .
                        'AND viu.`Day` <= ' . strtotime($dateVTo) . ' ' .
                'INNER JOIN Article_Categories AS ac ON ac.ArticleID = art.ID ' .
                'WHERE art.`Status` = \'Publish\' AND art.Stage = \'Publish\' '.
                        'AND art.PublishTime <= \'' . $dateTo . '\' ' .
                        'AND art.PublishTime > \'' . $dateFrom . '\' ' .
                        'AND ac.CategoryID IN (' . implode(', ', $categoryIds) . ') ' .
                'ORDER BY viu.Hit DESC ' .
                'LIMIT 0, ' . $limit;

        $result = $this->di->getShared('db')->query($sql);
        $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);

        $articles = $result->fetchAll();
        $memcached->save($cachedKey, $articles, 1 * 60 * 60);   // Cache for 1 hours

        return $articles;
    }

    /**
     * Get latest video and photo articles of a category
     *
     * @return array - List of articles
     */
    public function getLatestPhotosAndVideos($mainCategoryId, $categoryIds = [], $limit, $types = ['video', 'photo'])
    {
        $memcached = $this->di->getShared('memcached');

        $cachedKey = LATEST_PHOTOS_VIDEOS_CATEGORY_PREFIX . $mainCategoryId . implode('_', $types);
        $articles = $memcached->get($cachedKey);
        if (!empty($articles)) {
            return $articles;
        }

        $toDate = date('Y-m-d H:i:s');
        $fromDate = date('Y-m-d H:i:s', strtotime('-60 days'));

        $sql = 'SELECT art.ID as id, art.Title as title, art.PublishTime as publishTime, art.MetaSlug as metaSlug, art.ImageID as imageId, art.MetaTitle as metaTitle, art.LastEdited as lastEdited, art.ContentType as contentType, art.AllowComment as allowComment, art.ShowComment as showComment, art.CommentCount as commentCount, art.Sapo as sapo ' .
                'FROM Article art ' .
                'INNER JOIN Article_Categories ac ON ac.ArticleID = art.ID ' .
                'WHERE art.`Status` = \'Publish\' AND art.Stage = \'Publish\' ' .
                    'AND art.ContentType IN (\''.implode("', '", $types).'\') ' .
                    'AND art.PublishTime <= \'' . $toDate . '\' ' .
                    'AND art.PublishTime >= \'' . $fromDate . '\' ' .
                    'AND ac.CategoryID IN (' . implode(', ', $categoryIds) . ') ' .
                'ORDER BY PublishTime DESC ' .
                'LIMIT 0, ' . $limit;
        $result = $this->di->getShared('db')->query($sql);
        $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);

        $articles = $result->fetchAll();
        $memcached->save($cachedKey, $articles, 30 * 60);   // Cache for 30 minutes

        return $articles;
    }
}