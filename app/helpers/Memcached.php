<?php
namespace Ixosoftware\Cms\Helpers;

use Ixosoftware\Cms\Models\Category;
use Ixosoftware\Cms\Models\Pin;
use Ixosoftware\Cms\Models\Poll;
use Phalcon\Db;

define('ALL_CATEGORIES_KEY', 'AllCategories');
define('TOP_CATEGORIES_KEY', 'TopCategories');
define('POLL_KEY_PREFIX', 'Poll_');
define('PINED_ARTICLES_FOR_CATEGORIES', 'PinedArticlesForCategories');
define('MOST_READ_ARTICLES_CATEGORY_PREFIX', 'MostReadArticlesCategory_');
define('LATEST_PHOTOS_VIDEOS_CATEGORY_PREFIX', 'LatestPhotosAndVideos_');

class Memcached {
    public static function getTopCategories($limit = 9)
    {
        $di = \Phalcon\Di::getDefault();
        $config = $di->getShared('config');
        $memcached = $di->getShared('memcached');

        $categories = $memcached->get(TOP_CATEGORIES_KEY);
        if (!empty($categories)) {
            return $categories;
        }

        $categories = Category::query()
            ->columns('id,parentId')
            ->where('status = \'Active\'')
            ->orderBy('priority desc')
            ->limit($limit)
            ->execute();

        $memcached->save(TOP_CATEGORIES_KEY, $categories, $config->memcached->defaults->all_categories);

        return $categories;
    }

    public static function getAllCategories()
    {
        $di = \Phalcon\Di::getDefault();
        $config = $di->getShared('config');
        $memcached = $di->getShared('memcached');

        $categories = $memcached->get(ALL_CATEGORIES_KEY);
        if (!empty($categories)) {
            return $categories;
        }

        $result = Category::query()
            ->columns('id, parentId, title, metaTitle, description, metaDescription, metaSlug, metaKeyword, 
                        imageId, pinedEventId, adsJson, pinedPollId, gaTrackingCode, ordering, created, lastEdited')
            ->where('status = \'Active\'')
            ->execute();

        $categories = [];

        if ($result->count()) {
            foreach ($result as $category) {
                $category = $category->toArray();
                $category['slug'] = Scaffolding::getCategorySlug($category['metaSlug']);
                $categories[$category['id']] = (object)$category;
            }

            $memcached->save(ALL_CATEGORIES_KEY, $categories, $config->memcached->defaults->all_categories); // Cache categories for 2 hours
        }

        return $categories;
    }

    /**
     * Cache all pined articles for all categories
     */
    public static function getCategoryPinedArticles()
    {
        $di = \Phalcon\Di::getDefault();
        $config = $di->getShared('config');
        $memcached = $di->getShared('memcached');

        $pinedArticles = $memcached->get(PINED_ARTICLES_FOR_CATEGORIES);
        if (!empty($pinedArticles)) {
            return $pinedArticles;
        }

        $pinedArticles = [];

        $result = Pin::query()
            ->orderBy('categoryId asc, ordering asc')
            ->execute();
        if ($result->count()) {
            foreach ($result as $item) {
                if (!isset($pinedArticles[$item->categoryId])) {
                    $pinedArticles[$item->categoryId] = [];
                }
                $pinedArticles[$item->categoryId][] = $item->articleId;
            }

            $memcached->save(PINED_ARTICLES_FOR_CATEGORIES, $pinedArticles, $config->memcached->defaults->pined_articles);
        }
    }

    public static function getPollById($pollId)
    {
        $di = \Phalcon\Di::getDefault();
        $config = $di->getShared('config');
        $memcached = $di->getShared('memcached');

        $cachedKey = POLL_KEY_PREFIX . $pollId;
        $poll = $memcached->get($cachedKey);
        if (!empty($poll)) {
            return $poll;
        }

        $result = Poll::query()
            ->where('status = \'Active\'')
            ->andWhere('id=:id:')
            ->bind(['id' => $pollId])
            ->limit(1)
            ->execute();
        if ($result->count()) {
            $poll = $result->getFirst()->toArray();
            $poll['content'] = json_decode($poll['content'], true);
            $memcached->save($cachedKey, $poll, $config->memcached->defaults->poll_info);

            return $poll;
        }
        return null;
    }
}