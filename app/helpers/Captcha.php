<?php
namespace Ixosoftware\Cms\Helpers;

use Gregwar\Captcha\CaptchaBuilder;

define('CAPTCHA_KEY', 'reCaptcha_phrase');

class Captcha
{
    private $di;

    public function __construct()
    {
        $this->di = \Phalcon\Di::getDefault();
    }

    public function generateNewCaptcha()
    {
        $builder = new CaptchaBuilder();
        $builder->build();
        $builder->output(100);
        $this->di->getShared('session')->set(CAPTCHA_KEY, $builder->getPhrase());
    }

    public function testCaptcha($phrase)
    {
        $oldCaptcha = $this->di->getShared('session')->get(CAPTCHA_KEY);

        $this->di->getShared('session')->remove(CAPTCHA_KEY);

        if (null !== $oldCaptcha && null !== $phrase && $oldCaptcha == $phrase) {
            return true;
        }
        return false;
    }

    public function getCurrentPhrase()
    {
        return $this->di->getShared('session')->get(CAPTCHA_KEY);
    }
}