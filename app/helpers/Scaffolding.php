<?php
namespace Ixosoftware\Cms\Helpers;

class Scaffolding
{
    public static function getViewName($name)
    {
        $newName = mb_strtolower($name[0], 'UTF-8');
        for ($i = 1; $i < mb_strlen($name); $i++) {
            $char = $name[$i];
            $lowerChar = mb_strtolower($char, 'UTF-8');
            if ($lowerChar != $char) {
                $newName .= '-';
            }
            $newName .= $lowerChar;
        }
        return $newName;
    }

    public static function getCategorySlug($slug, $page = 1)
    {
        if (!empty($page) && $page > 1) {
            return sprintf('/c/%s/page/%d', $slug, $page);
        }
        return sprintf('/c/%s', $slug);
    }

    public static function getArticleSlug($slug, $id, $type = 'html')
    {
        if ($type == 'amp') {
            return sprintf('/%s-a%d.amp', $slug, $id);
        }
        return sprintf('/%s-a%d.html', $slug, $id);
    }

    public static function getTagSlug($slug, $page = 1)
    {
        if (!empty($page) && $page > 1) {
            return sprintf('/tag/%s/page/%d', $slug, $page);
        }
        return sprintf('/tag/%s', $slug);
    }

    public static function getEventSlug($eventId, $eventSlug, $page = 1)
    {
        if (!empty($page) && $page > 1) {
            return sprintf('/ev%s/%s/page/%d.html', $eventId, $eventSlug, $page);
        }
        return sprintf('/ev%s/%s.html', $eventId, $eventSlug);
    }
}