<?php
namespace Ixosoftware\Cms\Helpers;

class ComplexSearch
{
    /**
     * Get top articles from an event
     *
     * @return array - Top N article IDs from given event
     */
    public function getTopArticleIdsFromEvent($eventId, $ignoreArticleIds = [], $offset = 0, $limit = 5)
    {
        $di = \Phalcon\Di::getDefault();
        $serverConfig = $di->getShared('config')->elasticSearch->toArray();
        $results = NdtElasticSearch::getInstance($serverConfig)->searchArticlesByEvent($eventId, $ignoreArticleIds,$offset, $limit);

        return $results['articleIds'];
    }

    /**
     * Load articles by box type
     *
     * @return array - List of article IDs
     */
    public function loadArticleIdsByBoxType($boxType)
    {
        $di = \Phalcon\Di::getDefault();
        $serverConfig = $di->getShared('config')->elasticSearch->toArray();
        $articleIds = [];

        switch ($boxType) {
            case 'Side':
                $articleIds = NdtElasticSearch::getInstance($serverConfig)->newestArticles(null, 9);
                break;

            case 'Tindocnhieu':
                $articleIds = $this->tinDocNhieu($serverConfig, 10);
                break;

            case 'Binhluannhieu':
                $articleIds = $this->binhLuanNhieu($serverConfig, 9);
                break;

            case 'Tieudiemngay':
                $articleIds = $this->tieuDiemNgay($serverConfig, 9);
                break;

            default:
                break;
        }

        return $articleIds;
    }

    /**
     * Luật mới:
     * Tin đọc nhiều là danh sách đọc nhiều nhất trong 24h qua tính từ 7h sáng nay tới 7h sáng hôm sau
     * của những bài xuất bản trong vòng 72h qua
     * Không phân biệt chuyên mục nào nữa
     */
    private function tinDocNhieu($serverConfig, $limit = 10)
    {
        $endTime = strtotime('midnight');
        $startTime = date(\DateTime::ISO8601, strtotime('-3 days', $endTime));
        $endTime = date(\DateTime::ISO8601, strtotime('now'));
        return NdtElasticSearch::getInstance($serverConfig)->mostViewsInCategories(null, $startTime, $endTime, $limit);
    }

    /**
     * Bài viết Bình luận nhiều nhất ngày hôm qua
     * Gioi han mac dinh 9 bai
     */
    private function binhLuanNhieu($serverConfig, $limit = 9)
    {
        $beginOfDay = strtotime('midnight');
        $endOfDay = strtotime('tomorrow', strtotime('midnight')) - 1;

        $beginOfDay = date(\DateTime::ISO8601, strtotime('-1 day', $beginOfDay));
        $endOfDay = date(\DateTime::ISO8601, strtotime('-1 day', $endOfDay));

        return NdtElasticSearch::getInstance($serverConfig)->mostCommentedArticles(null, $beginOfDay, $endOfDay, $limit);
    }

    private function tieuDiemNgay($serverConfig, $limit = 9)
    {
        $categories = Memcached::getTopCategories($limit);

        $categoryIds = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categoryId = $category->id;
                $parentId = $category->parentId;

                if ($parentId) {
                    if (!isset($categoryIds[$parentId])) {
                        $categoryIds[$parentId] = [$parentId];
                    }
                    $categoryIds[$parentId][] = $categoryId;
                } else {
                    if (!isset($categoryIds[$categoryId])) {
                        $categoryIds[$categoryId] = [$categoryId];
                    }
                }
            }
        }

        $endOfDay = date(\DateTime::ISO8601, strtotime('now'));
        $beginOfDay = date(\DateTime::ISO8601, strtotime('-24 hours'));

        $articleIds = [];
        foreach ($categoryIds as $parentId => $subcategoryIds) {
            $categoryMostViewedArticleIds = NdtElasticSearch::getInstance($serverConfig)
                ->mostViewsInCategories($subcategoryIds, $beginOfDay, $endOfDay, 1);
            if (!empty($categoryMostViewedArticleIds)) {
                $articleIds[] = $categoryMostViewedArticleIds[0];
            }
        }

        return $articleIds;
    }
}