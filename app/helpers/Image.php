<?php
namespace Ixosoftware\Cms\Helpers;

class Image {
    public function generateThumbLink($imageUrl, $width = null, $height = null)
    {
        if (empty($width) && empty($height)) {
            return $imageUrl;
        }

        $thumbPortion = sprintf('/thumb_x%sx%s', $width?:'', $height?:'');

        if (($startPos = strpos($imageUrl, '/thumb_x')) !== false) {
            $endPos = strpos($imageUrl, '/', $startPos + 2);
            return substr_replace($imageUrl, $thumbPortion, $startPos, ($endPos - $startPos));
        }
        return str_replace('/media/', $thumbPortion.'/media/', $imageUrl);
    }

    public function getCustomDesktopSize($imageUrl, $size = 'not_set')
    {
        if (empty($imageUrl)) {
            return null;
        }

        switch ($size) {
            case 'wide_large' :
                $width = 580;
                $height = 302;
                break;
            case 'wide_medium' :
                $width = 240;
                $height = 136;
                break;
            case 'large' :
                $width = 372;
                $height = 232;
                break;
            case 'medium' :
                $width = 268;
                $height = 166;
                break;
            case 'small' :
                $width = 210;
                $height = 130;
                break;
            case 'squad' :
                $width = 300;
                $height = 300;
                break;
            default:
                $width = null;
                $height = null;
                break;
        }

        return $this->generateThumbLink($imageUrl, $width, $height);
    }
}