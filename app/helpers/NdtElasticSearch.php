<?php
namespace Ixosoftware\Cms\Helpers;
use \DateTime as DateTime;

class NdtElasticSearch
{
    private $INDEX = 'nguoiduatin';

    private static $instance = null;

    public static function getInstance($serverConfig = [])
    {
        if (self::$instance == null) {
            self::$instance = new NdtElasticSearch($serverConfig);
        }
        return self::$instance;
    }

    private $elasticSearch;

    private $defaultResults = ['total' => 0, 'hits' => []];

    public function __construct($serverConfig = [])
    {
        $this->elasticSearch = new \Elastica\Client($serverConfig);
        $this->INDEX = isset($serverConfig['index_name']) ? $serverConfig['index_name'] : $this->INDEX;
    }

    /**
     * Lấy bài đọc nhiều nhất trong các categories được truyền vào
     * Ví dụ: Nếu truyền vào $categoryIds = ["1", "2", "3"] thì sẽ lấy N bài đọc nhiều nhất từ tất cả các cateogries này
     * Mảng ID sẽ có lợi khi muốn lấy bài đọc nhiều nhất trong 1 category và các categories con của nó
     *
     * Nếu không có categoryIds nào được truyền vào, mặc định lấy top N bài viết đọc nhiều nhất từ tất cả các category
     *
     * -----------------------
     * Hướng dẫn sử dụng:
     * $results = NdtElasticSearch::getInstance()->mostViewsInCategories(["1", "2", "3"], '2017-06-10', '2016-06-12T12:14:16.000Z', 5);
     * -----------------------
     *
     * @param array $categoryIds
     * @param date $from - Format 'Y-m-d' hoặc ISO8601 (@ref: http://php.net/manual/en/function.date.php)
     * @param date $to - Format 'Y-m-d' hoặc ISO8601 (@ref: http://php.net/manual/en/function.date.php)
     * @param int $numResults - Number of results
     * @return array - With 2 keys: "total" and an array of results "hits"
     */
    public function mostViewsInCategories($categoryIds = [], $from, $to, $numResults = 5)
    {
        if (!$this->elasticSearch) {
            return $this->defaultResults;
        }

        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'pageview' => ['order' => 'desc']
            ],
            'from' => 0,
            'size' => $numResults,
            '_source' => ['id', 'pageview', 'title']
        ];

        // Filter by categories
        if (!empty($categoryIds)) {
            $shouldMatches = [];
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }

            $query['query'] = [
                'bool' => [
                    'should' => $shouldMatches,
                    'minimum_should_match' => 1,
                    'filter' => [
                        'bool' => [
                            'must' => [
                                [
                                    'range' => [
                                        'publishtime' => [
                                            'gte' => $from,
                                            'lte' => $to
                                        ]
                                    ]
                                ],
                                [
                                    'term' => [
                                        'showinnew' => true
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        } // Get from all categories
        else {
            $query['query'] = [
                'bool' => [
                    'must' => [
                        [
                            'range' => [
                                'publishtime' => [
                                    'gte' => $from,
                                    'lte' => $to
                                ]
                            ]
                        ],
                        [
                            'term' => [
                                'showinnew' => true
                            ]
                        ]
                    ]
                ]
            ];
        }

        $results = $this->executeQuery($query, 'GET', $path);
        $articleIds = [];

        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_id'];
                }
                else {
                    $articleIds[] = $hit['_source']['id'];
                }
            }
        }

        return $articleIds;
    }

    /**
     * Tìm kiếm bài báo phù hợp nhất với từ khóa dựa theo title
     * Nếu không có categoryIds nào được truyền vào thì tìm từ tất cả các categories
     *
     * @param $queryStr
     * @param $categoryIds
     * @param int $offset
     * @param int $numResults
     * @return array
     */
    public function searchArticlesByTitle($queryStr, $categoryIds = [], $offset = 0, $numResults = 5)
    {
        if (!$this->elasticSearch || empty($queryStr)) {
            return $this->defaultResults;
        }

        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'track_scores' => true,
            'sort' => [
                '_score' => ['order' => 'desc'],
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $offset,
            'query' => [
                'multi_match' => [
                    'type' => 'phrase',
                    'query' => $queryStr,
                    'fields' => ['title']
                ]
            ]
        ];

        if (!empty($categoryIds)) {
            $shouldMatches = [];
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }
            $query['filter'] = [
                'bool' => [
                    'should' => $shouldMatches,
                    'minimum_should_match' => 1
                ]
            ];
        }

        return $this->executeQuery($query, 'GET', $path);
    }

    public function searchArticles($queryStr, $offset = 0, $numResults = 5)
    {
        if (!$this->elasticSearch || empty($queryStr)) {
            return $this->defaultResults;
        }

        $newQueryStrs = $this->formatQueryString($queryStr);

        $matches = [];
        if (count($newQueryStrs) > 1) {
            $shouldMatches = [];
            foreach ($newQueryStrs as $str) {
                $shouldMatches[] = [
                    'multi_match' => [
                        'type' => 'phrase',
                        'query' => $str,
                        'fields' => ['title^6', 'sapo^3', 'content']
                    ]
                ];
            }
            $matches['should'] = $shouldMatches;
            $matches['minimum_should_match'] = 1;
        }
        else {
            $matches['must'] = [
                'multi_match' => [
                    'type' => 'phrase',
                    'query' => $newQueryStrs[0],
                    'fields' => ['title^6', 'sapo^3', 'content']
                ]
            ];
        }

        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'track_scores' => true,
            'sort' => [
                '_score' => ['order' => 'desc'],
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $offset,
            '_source' => 'id',
            'query' => [
                'function_score' => [
                    'query' => [
                        'bool' => [
                            /*'must' => [
                                'multi_match' => [
                                    'type' => 'phrase',
                                    'query' => $queryStr,
                                    'fields' => ['title^6', 'sapo^3', 'content']
                                ]
                            ],*/
                            "filter" => [
                                'range' => [
                                    'publishtime' => [
                                        'lte' => date(DateTime::ISO8601)
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'script_score' => [
                        'script' => 'double days = Math.floor(new Date(doc["publishtime"].value).getTime() / 86400000); double today = Math.floor(new Date().getTime() / 86400000); double _30daysago = today - 30; double diff = days - _30daysago; days = diff > 2 ? diff : 2; return _score * Math.log(days)'
                    ]
                ]
            ]
        ];

        $query['query']['function_score']['query']['bool'] = array_merge($query['query']['function_score']['query']['bool'], $matches);

        /*echo '<pre>';
        print_r($query);
        die();*/

        return $this->executeQuery($query, 'GET', $path);
    }

    /**
     * Count all published articles in selected categories
     *
     * @param array $categoryIds
     * @return int - Total articles or 0 if nothing found
     */
    public function countPublishedArticlesInCategory($categoryIds = [])
    {
        if (!$this->elasticSearch || empty($categoryIds)) {
            return $this->defaultResults;
        }

        $path = sprintf('%s/%s/_count', $this->INDEX, IndexTypes::ARTICLES);

        $shouldMatches = [];
        foreach ($categoryIds as $categoryId) {
            $shouldMatches[] = [
                'match' => [
                    'categoryids' => (string)$categoryId
                ]
            ];
        }

        $query = [
            'query' => [
                'bool' => [
                    'should' => $shouldMatches,
                    'minimum_should_match' => 1,
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(DateTime::DATE_ISO8601)
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $this->executeCountQuery($query, $path, 'GET');
    }

    /**
     * Lấy danh sách $numResults bài mới nhất từ:
     *  - Danh sách $categoryIds được truyền vào
     *  - Từ tất cả các categories nếu $categoryIds không được truyền vào
     *
     * @param array $categoryIds
     * @param int $numResults
     * @return array - List of article ids
     */
    public function newestArticles($categoryIds = [], $numResults = 10, $from = 0)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'track_scores' => true,
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $from,
            '_source' => 'id',
            'query' => [
                'bool' => [
                    'must' => [
                        'term' => [
                            'showinnew' => true
                        ]
                    ],
                    "filter" => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(\DateTime::ISO8601)
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (!empty($categoryIds)) {
            $shouldMatches = [];
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }
            $query['query']['bool']['should'] = $shouldMatches;
            $query['query']['bool']['minimum_should_match'] = 1;
        }

        $articleIds = [];

        $results = $this->executeQuery($query, 'GET', $path);

        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_id'];
                }
                else {
                    $articleIds[] = $hit['_source']['id'];
                }
            }
        }
        return $articleIds;
    }

    public function newestArticlesInCategorys($categoryIds = [], $numResults = 10, $from = 0)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'track_scores' => true,
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $from,
            '_source' => 'id',
            'query' => [
                'bool' => [
                    "filter" => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(DateTime::ISO8601)
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (!empty($categoryIds)) {
            $shouldMatches = [];
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }
            $query['query']['bool']['should'] = $shouldMatches;
            $query['query']['bool']['minimum_should_match'] = 1;
        }

        $articleIds = [];

        $results = $this->executeQuery($query, 'GET', $path);

        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_id'];
                }
                else {
                    $articleIds[] = $hit['_source']['id'];
                }
            }
        }
        return $articleIds;
    }

    /**
     * Lấy danh sách các bài viết được comment nhiều nhất trong khoảng thời gian từ $start tới $end
     * Nếu muốn giới hạn trong một category (và subcategories) nào thì truyền vào mảng $categoryIds
     *
     * @param array $categoryIds
     * @param int $numResults
     * @return array - List of article ids
     */
    public function mostCommentedArticles($categoryIds = [], $start, $end, $numResults = 10)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::COMMENTS);

        $query = [
            'size' => 0,    // Don't retrieve query results, only aggregations
            'query' => [
                'bool' => [
                    'filter' => [
                        'range' => [
                            'created' => [
                                'gte' => $start,
                                'lte' => $end
                            ]
                        ]
                    ]
                ]
            ],
            'aggregations' => [
                'group_by_article_id' => [
                    'terms' => [
                        'field' => 'articleid',
                        'size' => $numResults
                    ]
                ]
            ]
        ];

        if (!empty($categoryIds)) {
            $shouldMatches = [];
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }
            $query['query']['bool']['should'] = $shouldMatches;
            $query['query']['bool']['minimum_should_match'] = 1;
        }

        $articleIds = [];
        $results = $this->executeQuery($query, 'GET', $path);

        if (!empty($results['aggregations']['group_by_article_id']['buckets'])) {
            $buckets = $results['aggregations']['group_by_article_id']['buckets'];

            foreach ($buckets as $bucket) {
                $articleIds[] = $bucket['key'];
            }
        }

        return $articleIds;
    }

    /**
     * Tìm kiếm danh sách article IDs tương ứng với một tag
     *
     * @param $tagId
     * @param int $offset
     * @param int $numResults
     * @return array List of ArticleIDs
     */
    public function searchArticlesByTag($tagId, $offset = 0, $numResults = 10)
    {
        if (!$this->elasticSearch || empty($tagId)) {
            return ['total' => 0, 'articleIds' => []];
        }

        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $offset,
            '_source' => ['id'],
            'query' => [
                'bool' => [
                    'should' => [
                        ['match' => ['tagids' => (string)$tagId]]
                    ],
                    'minimum_should_match' => 1,
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(DateTime::ISO8601)
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $articleIds = [];

        $results = $this->executeQuery($query, 'GET', $path);

        $total = $results['total'];

        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (!empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_source']['id'];
                }
                else {
                    $articleIds[] = $hit['_id'];
                }
            }
        }
        return [
            'total' => $total,
            'articleIds' => $articleIds
        ];
    }

    /**
     * Tìm kiếm các bài viết trong cùng sự kiện, sắp xếp theo thứ tự thời gian
     *
     * @param $eventId
     * @param int $offset
     * @param int $numResults
     * @return array
     */
    public function searchArticlesByEvent($eventId, $excludedIds = [], $offset = 0, $numResults = 10)
    {
        if (!$this->elasticSearch || empty($eventId)) {
            return ['total' => 0, 'articleIds' => []];
        }

        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $offset,
            '_source' => ['id'],
            'query' => [
                'bool' => [
                    'should' => [
                        ['match' => ['eventids' => $eventId]]
                    ],
                    'minimum_should_match' => 1,
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(\DateTime::ISO8601)
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (!empty($excludedIds)) {
            $query['query']['bool']['must_not'] = [
                'terms' => [
                    '_id' => $excludedIds
                ]
            ];
        }

        $articleIds = [];

        $results = $this->executeQuery($query, 'GET', $path);

        $total = $results['total'];

        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (!empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_source']['id'];
                }
                else {
                    $articleIds[] = $hit['_id'];
                }
            }
        }
        return [
            'total' => $total,
            'articleIds' => $articleIds
        ];
    }

    /**
     * Tìm kiếm danh sách các bài viết cùng chuyên mục, sắp xếp theo tiêu chí thời gian
     *
     * @param array $categoryIds
     * @param array $excludedArticleIds
     * @param int Number of articles want to get from ElasticSearch
     * @return array List of Article IDs
     */
    public function relatedArticlesInCategory($categoryIds = [], $excludedArticleIds = [], $numResults = 10)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => 0,
            '_source' => 'id',
            'query' => [
                'bool' => [
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(\DateTime::ISO8601, strtotime('now'))
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $shouldMatches = [];

        if (!empty($categoryIds)) {
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }

            $query['query']['bool']['should'] = $shouldMatches;
            $query['query']['bool']['minimum_should_match'] = 1;
        }

        if (!empty($excludedArticleIds)) {
            $query['query']['bool']['must_not'] = [
                'terms' => [
                    '_id' => $excludedArticleIds
                ]
            ];
        }


        $results = $this->executeQuery($query, 'GET', $path);

        $articleIds = [];
        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (!empty($hit['_source']['id'])) {
                    $articleIds[] = $hit['_source']['id'];
                }
                else {
                    $articleIds[] = $hit['_id'];
                }
            }
        }

        return [
            'total' => $results['total'],
            'articleIds' => $articleIds
        ];
    }

    /**
     * Tìm kiếm danh sách các bài viết cùng chuyên mục, sắp xếp theo tiêu chí thời gian
     * Cập nhật phân trang
     *
     * @param array $categoryIds
     * @param array $excludedArticleIds
     * @param int Number of articles want to get from ElasticSearch
     * @return array List of Article IDs
     */
    public function relatedArticlesInCategories($categoryIds = [], $excludedArticleIds = [], $numResults = 10, $from = 0)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $numResults,
            'from' => $from,
            '_source' => 'id',
            'query' => [
                'bool' => [
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(DateTime::ISO8601, strtotime('now'))
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $shouldMatches = [];

        if (!empty($categoryIds)) {
            foreach ($categoryIds as $categoryId) {
                $shouldMatches[] = [
                    'match' => [
                        'categoryids' => (string)$categoryId
                    ]
                ];
            }

            $query['query']['bool']['should'] = $shouldMatches;
            $query['query']['bool']['minimum_should_match'] = 1;
        }

        if (!empty($excludedArticleIds)) {
            $query['query']['bool']['must_not'] = [
                'terms' => [
                    '_id' => $excludedArticleIds
                ]
            ];
        }

        $results = $this->executeQuery($query, 'GET', $path);

        $articleIds = [];
        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (isset($hit['_source']['id'])) {
                    $articleIds[] = $hit['_source']['id'];
                }
                else {
                    $articleIds[] = $hit['_id'];
                }
            }
        }

        return [
            'total' => $results['total'],
            'articleIds' => $articleIds
        ];
    }

    public function RelatedArticlesByAuthor($authorId, $excludedIds = [], $offset = 0, $limit = 10)
    {
        $path = sprintf('%s/%s/_search', $this->INDEX, IndexTypes::ARTICLES);

        $query = [
            'sort' => [
                'publishtime' => ['order' => 'desc']
            ],
            'size' => $limit,
            'from' => $offset,
            '_source' => 'id',
            'query' => [
                'bool' => [
                    'must' => [
                        'term' => [
                            'journalistid' => $authorId
                        ]
                    ],
                    'filter' => [
                        'range' => [
                            'publishtime' => [
                                'lte' => date(DateTime::ISO8601, strtotime('now'))
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (!empty($excludedIds)) {
            $query['query']['bool']['must_not'] = [
                'terms' => [
                    '_id' => $excludedIds
                ]
            ];
        }

        $results = $this->executeQuery($query, 'GET', $path);

        $articleIds = [];
        if (!empty($results['hits'])) {
            foreach ($results['hits'] as $hit) {
                if (isset($hit['_source']['id'])) {
                    $articleIds[] = $hit['_source']['id'];
                }
                else {
                    $articleIds[] = $hit['_id'];
                }
            }
        }

        return [
            'total' => $results['total'],
            'articleIds' => $articleIds
        ];
    }

    private function formatQueryString($queryString) {
        if (empty($queryString)) {
            return $queryString;
        }
        $queryString = $this->convertVietnamseToEnglish($queryString);
        $queryString = strtolower($queryString);

        $replacements = [
            '20 thang 11' => ['20/11', '20-11', '20 thang 11', 'ngay nha giao viet nam'],
            'ngay nha giao viet nam' => ['20/11', '20-11', '20 thang 11', 'ngay nha giao viet nam'],

            '8 thang 3' => ['8/3', '8-3', '08/03', '8 thang 3', '08 thang 03', 'quoc te phu nu'],
            '08 thang 03' => ['8/3', '8-3', '08/03', '8 thang 3', '08 thang 03', 'quoc te phu nu'],
            'quoc te phu nu' => ['8/3', '8-3', '08/03', '8 thang 3', '08 thang 03', 'quoc te phu nu'],

            '2 thang 9' => ['2/9', '02/09', '2-9', '2 thang 9', '02 thang 09', 'quoc khanh'],
            'quoc khanh' => ['2/9', '02/09', '2-9', '2 thang 9', '02 thang 09', 'quoc khanh'],

            '20 thang 10' => ['20/10', '20-10', '20 thang 10', 'ngay phu nu viet nam'],
            'ngay phu nu viet nam' => ['20/10', '20-10', '20 thang 10', 'ngay phu nu viet nam'],

            '30 tháng 04' => ['30/4', '30/04', '30-4', '30-04', '30 thang 4', '30 thang 04', 'giai phong mien nam'],
            'giai phong mien nam' => ['30/4', '30/04', '30-4', '30-04', '30 thang 4', '30 thang 04', 'giai phong mien nam'],

            '1 thang 5' => ['1/5', '01/05', '1-5', '01-05', '1 thang 5', '01 thang 05', 'quoc te lao dong'],
            '01 thang 05' => ['1/5', '01/05', '1-5', '01-05', '1 thang 5', '01 thang 05', 'quoc te lao dong'],
            'quoc te lao dong' => ['1/5', '01/05', '1-5', '01-05', '1 thang 5', '01 thang 05', 'quoc te lao dong'],

            '27 thang 7' => ['27/7', '27/7', '27 thang 7', 'ngay thuong binh liet si'],
            'ngay thuong binh liet si' => ['27/7', '27/7', '27 thang 7', 'ngay thuong binh liet si'],
            'thuong binh liet si' => ['27/7', '27/7', '27 thang 7', 'thuong binh liet si'],

            '10 thang 10' => ['10/10', '10-10', '10 thang 10', 'giai phong thu do'],
            'giai phong thu do' => ['10/10', '10-10', '10 thang 10', 'giai phong thu do'],

            '10 thang 3' => ['10/3', '10/03', '10 thang 3', 'giỗ tổ hùng vương'],
            'gio to hung vuong' => ['10/3', '10/03', '10 thang 3', 'giỗ tổ hùng vương'],
        ];

        $results = [$queryString];

        foreach ($replacements as $search => $replaces) {
            if (strpos($queryString, $search) !== FALSE) {
                $newQueries = [];
                foreach ($replaces as $str) {
                    $newQueries[] = str_ireplace($search, $str, $queryString);
                }
                $results = array_merge($results, $newQueries);
            }
        }

        return array_unique($results);
    }

    private function convertVietnamseToEnglish($str) {
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
        $str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
        $str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
        $str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
        $str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
        $str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
        $str = preg_replace('/(Đ)/', 'D', $str);
        //$str = str_replace(' ', '-', str_replace('&*#39;','",$str));
        return $str;
    }

    private function executeQuery($query, $method, $path)
    {
        try {
            $response = $this->elasticSearch->request($path, $method, $query);
            if ($response->getStatus() == 200) {
                $responseData = $response->getData();
                $data = [
                    'total' => (int)$responseData['hits']['total'],
                    'hits' => $responseData['hits']['hits']
                ];

                if (isset($responseData['aggregations'])) {
                    $data['aggregations'] = $responseData['aggregations'];
                }

                return $data;
            }
        }
        catch (Exception $e) {
            /*echo $e->getMessage();
            echo '<pre>';
            print_r($e);
            die();*/

            return [
                'total' => 0,
                'hits' => []
            ];
        }
        return $this->defaultResults;
    }

    private function executeCountQuery($query, $path, $method)
    {
        $response = $this->elasticSearch->request($path, $method, $query);
        if ($response->getStatus() == 200) {
            $responseData = $response->getData();
            return (int)$responseData['count'];
        }
        return 0;
    }
}

class IndexTypes
{
    const ARTICLES = "article";
    const VIEWS = "views";
    const COMMENTS = "comments";
    const TAGS = "tags";
}