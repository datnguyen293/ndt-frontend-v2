<?php
namespace Ixosoftware\Cms\Helpers;

class Pagination
{
    private $itemsPerPage = 10;
    private $totalItems = 0;
    private $currentPage = 1;
    private $middleRange = 7;

    private $divClass = 'full-width vertical-items pull-left';
    private $ulClass = 'pagination';
    private $liClass = '';
    private $linkClass = '';
    private $linkActiveClass = 'active';

    private $prevLiClass = 'prev';
    private $nextLiClass = 'next';
    private $prevText = '⇤';
    private $nextText = '⇥';

    private $baseUrl;
    private $pageInQuery = false;
    private $pageSegmentName = 'page';
    private $otherParams = [];

    public function __construct()
    {
    }

    public function paginate()
    {
        if ($this->totalItems == 0 || $this->itemsPerPage == 0) {
            return null;
        }

        $totalPages = ceil($this->totalItems / $this->itemsPerPage);
        if ($totalPages == 1) {
            return null;
        }

        $output = '<div class="' . $this->divClass . '"><ul class="' . $this->ulClass . '">';

        if ($this->currentPage < 1) {
            $this->currentPage = 1;
        }
        else if ($this->currentPage > $totalPages) {
            $this->currentPage = $totalPages;
        }

        $nextPage = $this->currentPage + 1;
        $prevPage = $this->currentPage - 1;

        if ($nextPage > $totalPages) {
            $nextPage = $this->currentPage;
        }
        if ($prevPage < 1) {
            $prevPage = $this->currentPage;
        }

        if ($totalPages > $this->middleRange) {
            // Previous page
            if ($this->currentPage != 1 && $this->totalItems >= $this->itemsPerPage) {
                $output .= '<li class="'.$this->liClass.' '.$this->prevLiClass.'">';
                $output .= '<a class="' . $this->linkClass . '" href="'.$this->generatePageLink($prevPage).'">'.$this->prevText.'</a>';
                $output .= '</li>';
            }

            // Generate inner pages
            $startRange = $this->currentPage - 2;
            $endRange = $this->currentPage + 2;

            if ($startRange <= 0) {
                $endRange += abs($startRange) + 1;
                $startRange = 1;
            }
            if ($endRange > $totalPages) {
                $startRange -= $endRange - $totalPages;
                $endRange = $totalPages;
            }

            $range = range($startRange, $endRange);
            foreach ($range as $page) {
                if ($page == $this->currentPage) {
                    $output .= '<li class="'.$this->liClass.' active"><a class="'.$this->linkClass.' '.$this->linkActiveClass.'">'.$page.'</a></li>';
                }
                // Generate normal link
                else {
                    $output .= '<li class="'.$this->liClass.'"><a class="'.$this->linkClass.'" href="'.$this->generatePageLink($page).'">'.$page.'</a></li>';
                }
            }

            // Generate next page
            if ($this->currentPage != $totalPages && $this->totalItems >= $this->itemsPerPage) {
                $output .= '<li class="'.$this->liClass.' '.$this->nextLiClass.'">';
                $output .= '<a class="'.$this->linkClass.'" href="'.$this->generatePageLink($nextPage).'">'.$this->nextText.'</a>';
                $output .= '</li>';
            }
        }
        else {
            for ($i = 1; $i <= $totalPages; $i++) {
                if ($i == $this->currentPage) {
                    $output .= '<li class="'.$this->liClass.' active"><a class="'.$this->linkClass.' '.$this->linkActiveClass.'">'.$i.'</a></li>';
                }
                // Generate normal link
                else {
                    $output .= '<li class="'.$this->liClass.'"><a class="'.$this->linkClass.'" href="'.$this->generatePageLink($i).'">'.$i.'</a></li>';
                }
            }
        }

        $output .= '</ul></div>';

        return $output;
    }

    private function generatePageLink($page = 1)
    {
        if ($this->pageInQuery) {
            $query = '';
            if (!empty($this->otherParams)) {
                $query .= '&' . http_build_query($this->otherParams);
            }
            $query .= '&' . $this->pageSegmentName . '=' . $page;

            $url = $this->baseUrl . '?' . trim($query, '& ');

            return $url;
        }

        $url = $this->baseUrl . '/' . $this->pageSegmentName . '/' . $page;

        $stringUtils = new StringUtils();
        if ($stringUtils->endsWith($this->baseUrl, '.html')) {
            $pos = strpos($this->baseUrl, '.html');
            $url = substr($this->baseUrl, 0, $pos) . '/' . $this->pageSegmentName . '/' . $page . '.html';
        }

        if (!empty($this->otherParams)) {
            foreach ($this->otherParams as $key => $value) {
                $url .= '/' . $key . '/' . urlencode($value);
            }
        }
        return $url;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @return int
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @param int $totalItems
     */
    public function setTotalItems($totalItems)
    {
        $this->totalItems = $totalItems;
    }

    /**
     * @return int
     */
    public function getMiddleRange()
    {
        return $this->middleRange;
    }

    /**
     * @param int $middleRange
     */
    public function setMiddleRange($middleRange)
    {
        $this->middleRange = $middleRange;
    }

    /**
     * @return string
     */
    public function getDivClass()
    {
        return $this->divClass;
    }

    /**
     * @param string $divClass
     */
    public function setDivClass($divClass)
    {
        $this->divClass = $divClass;
    }

    /**
     * @return string
     */
    public function getUlClass()
    {
        return $this->ulClass;
    }

    /**
     * @param string $ulClass
     */
    public function setUlClass($ulClass)
    {
        $this->ulClass = $ulClass;
    }

    /**
     * @return string
     */
    public function getLiClass()
    {
        return $this->liClass;
    }

    /**
     * @param string $liClass
     */
    public function setLiClass($liClass)
    {
        $this->liClass = $liClass;
    }

    /**
     * @return string
     */
    public function getLinkClass()
    {
        return $this->linkClass;
    }

    /**
     * @param string $linkClass
     */
    public function setLinkClass($linkClass)
    {
        $this->linkClass = $linkClass;
    }

    /**
     * @return string
     */
    public function getLinkActiveClass()
    {
        return $this->linkActiveClass;
    }

    /**
     * @param string $linkActiveClass
     */
    public function setLinkActiveClass($linkActiveClass)
    {
        $this->linkActiveClass = $linkActiveClass;
    }

    /**
     * @return bool
     */
    public function isPageInQuery()
    {
        return $this->pageInQuery;
    }

    /**
     * @param bool $pageInQuery
     */
    public function setPageInQuery($pageInQuery)
    {
        $this->pageInQuery = $pageInQuery;
    }

    /**
     * @return string
     */
    public function getPageSegmentName()
    {
        return $this->pageSegmentName;
    }

    /**
     * @param string $pageSegmentName
     */
    public function setPageSegmentName($pageSegmentName)
    {
        $this->pageSegmentName = $pageSegmentName;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }


    /**
     * @return string
     */
    public function getPrevLiClass()
    {
        return $this->prevLiClass;
    }

    /**
     * @param string $prevLiClass
     */
    public function setPrevLiClass($prevLiClass)
    {
        $this->prevLiClass = $prevLiClass;
    }

    /**
     * @return string
     */
    public function getNextLiClass()
    {
        return $this->nextLiClass;
    }

    /**
     * @param string $nextLiClass
     */
    public function setNextLiClass($nextLiClass)
    {
        $this->nextLiClass = $nextLiClass;
    }


    /**
     * @return string
     */
    public function getPrevText()
    {
        return $this->prevText;
    }

    /**
     * @param string $prevText
     */
    public function setPrevText($prevText)
    {
        $this->prevText = $prevText;
    }

    /**
     * @return string
     */
    public function getNextText()
    {
        return $this->nextText;
    }

    /**
     * @param string $nextText
     */
    public function setNextText($nextText)
    {
        $this->nextText = $nextText;
    }


    /**
     * @return array
     */
    public function getOtherParams()
    {
        return $this->otherParams;
    }

    /**
     * @param array $otherParams
     */
    public function setOtherParams($otherParams)
    {
        $this->otherParams = $otherParams;
    }
}