<?php
namespace Ixosoftware\Cms\Helpers;

class URL
{
    private $di;
    private $config;

    public function __construct()
    {
        $this->di = \Phalcon\Di::getDefault();
        $this->config = $this->di->getShared('config');
    }

    public function getCurrentUrl()
    {
        return $this->di->getRouter()->getRewriteUri();
    }

    public function getCurrentDesktopUrl()
    {
        return sprintf('%s%s', $this->getDesktopDomain(), $this->getCurrentUrl());
    }

    public function getCurrentMobileUrl()
    {
        return sprintf('%s%s', $this->getMobileDomain(), $this->getCurrentUrl());
    }

    public function getDesktopUrl($uri)
    {
        return sprintf('%s/%s', $this->getDesktopDomain(), ltrim($uri, '/ '));
    }

    public function getMobileUrl($uri)
    {
        return sprintf('%s/%s', $this->getMobileDomain(), ltrim($uri, '/ '));
    }

    public function getFullUrl($uri, $type = 'desktop')
    {
        switch ($type) {
            case 'mobile':
            case 'amp':
                return $this->getMobileUrl($uri);

            default:
                return $this->getDesktopUrl($uri);
        }
    }

    public function getAssetUrl($uri)
    {
        return sprintf('%s/%s', $this->getAssetsDomain(), ltrim($uri, '/ '));
    }

    public function getDesktopDomain()
    {
        return $this->config->application->desktopDomain;
    }

    public function getMobileDomain()
    {
        return $this->config->application->mobileDomain;
    }

    public function getAssetsDomain()
    {
        return $this->config->application->assetsUrl;
    }
}