<?php
namespace Ixosoftware\Cms\Helpers;

class StringUtils
{
    public function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||
            (substr($haystack, -$length) === $needle);
    }

    public function escapeHtmlValue($string)
    {
        return htmlspecialchars($string);
    }
}