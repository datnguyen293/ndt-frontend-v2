<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\NdtElasticSearch;
use Ixosoftware\Cms\Helpers\Pagination;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Models\Event;

class EventController extends ControllerBase
{
    private $event;
    private $lastEdited;
    private $data = [];

    public function indexAction()
    {
        $eventId = $this->dispatcher->getParam('eventId');
        $eventSlug = $this->dispatcher->getParam('slug');

        if (empty($eventId)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Sự kiện không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }

        $this->event = Event::query()
            ->where('id=:eventId:')
            ->andWhere('status=:status:')
            ->bind(['eventId' => $eventId, 'status' => 'Active'])
            ->execute()
            ->getFirst();
        if (empty($this->event)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Sự kiện không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }

        if ($this->event->metaSlug != $eventSlug) {
            return $this->response->redirect(Scaffolding::getEventSlug($eventId, $this->event->metaSlug));
        }

        $page = $this->dispatcher->getParam('page');
        if ((empty($page) || $page <= 1) && strpos($this->router->getRewriteUri(), '/page/') !== false) {
            return $this->response->redirect(Scaffolding::getEventSlug($eventId, $this->event->metaSlug));
        }
        if (!$page) { $page = 1; }

        $imageIds = [];
        $imageIds[] = $this->event->imageId;

        $itemsPerPage = $this->config->defaults->numArticlesPerEventPage;
        $from = ($page - 1) * $itemsPerPage;

        $this->lastEdited = $this->event->lastEdited;

        $serverConfig = $this->config->elasticSearch->toArray();
        $results = NdtElasticSearch::getInstance($serverConfig)->searchArticlesByEvent($eventId, [], $from, $itemsPerPage);
        $total = $results['total'];

        $articleIds = $results['articleIds'];
        $this->getArticlesByIds($articleIds, $imageIds);

        $imageIds = array_unique($imageIds);
        $this->getImagesByIds($imageIds);

        $articles = $this->extractArticlesFromMapByIds($articleIds, $this->lastEdited);

        // Generate page links
        $baseUrl = Scaffolding::getEventSlug($this->event->id, $this->event->metaSlug);
        $paginationHelper = new Pagination();
        if ($this->isMobile) {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getMobileUrl($baseUrl));
        }
        else {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getDesktopUrl($baseUrl));
        }
        $paginationHelper->setTotalItems($total);
        $paginationHelper->setItemsPerPage($itemsPerPage);
        $paginationHelper->setCurrentPage($page);
        $paginationHelper->setPageSegmentName('page');
        $pageLinks = $paginationHelper->paginate();

        $this->data = [
            'articles' => $articles,
            'event' => $this->event->toArray(),
            'totalArticles' => $total,
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
            'eventLink' => Scaffolding::getEventSlug($eventId, $this->event->metaSlug, $page),
            'pageLinks' => $pageLinks
        ];

        // Show anchor text under article title
        if (!empty($this->event->anchorText) && !empty($this->event->anchorTextActive)) {
            $this->data['anchorText'] = $this->event->anchorText;
            if ($this->isMobile) {
                $this->data['anchorTextLink'] = $this->helpers['urlHelper']->getMobileUrl(Scaffolding::getEventSlug($this->event->id, $this->event->metaSlug));
            }
            else {
                $this->data['anchorTextLink'] = $this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getEventSlug($this->event->id, $this->event->metaSlug));
            }
        }

        $this->view->setVars($this->data);
    }

    function setMetaTags()
    {
        if (empty($this->event->metaTitle)) {
            $tagTitle = ucfirst($this->event->title . ' - Tin Tức về ' . $this->event->title . ' mới nhất');
        }
        else {
            $tagTitle = $this->event->metaTitle;
        }

        $appendedTitle = '';
        if ($this->data['totalArticles'] >= $this->data['itemsPerPage'] && $this->data['page'] > 1) {
            $appendedTitle .= ' - Trang ' . $this->data['page'];
        }

        if (empty($this->event->description)) {
            $tagDescription = ucfirst($this->event->title . ' - Tin tức ' . $this->event->title . '  cập nhật đầy đủ và mới nhất trên Báo Người Đưa Tin.');
        }
        else {
            $tagDescription = ucfirst($this->event->description);
        }

        if ($this->isMobile) {
            $tagLink = $this->helpers['urlHelper']->getMobileUrl($this->data['eventLink']);
        }
        else {
            $tagLink = $this->helpers['urlHelper']->getDesktopUrl($this->data['eventLink']);
        }

        $this->metaTags['MetaTitle']        = $tagTitle . $appendedTitle;
        $this->metaTags['MetaDescription']  = $tagDescription;
        $this->metaTags['MetaKeyword']      = $this->event->metaKeyword;
        $this->metaTags['MetaSlug']         = $tagLink;
        $this->metaTags['PageType']         = 'Tag';
        $this->metaTags['PublishTime']      = $this->event->created;
        $this->metaTags['LastEdited']       = date('Y-m-d H:i:s', $this->lastEdited);
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}