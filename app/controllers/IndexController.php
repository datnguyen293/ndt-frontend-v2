<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\Memcached;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Models\Article;
use Ixosoftware\Cms\Models\Event;
use Ixosoftware\Cms\Models\File;
use Ixosoftware\Cms\Models\HomePage;
use Ixosoftware\Cms\Models\HomePageBoxData;
use Ixosoftware\Cms\Models\Member;
use Phalcon\Exception;

class IndexController extends ControllerBase
{
    private $categoriesMap = [];
    private $eventsMap = [];
    private $pollsMap = [];
    private $journalistsMap = [];
    private $boxesMap = [];
    private $boxTypesArticlesMap = [];

    private $categoryHelper;

    private $lastEdited;
    private $created;

    public function initialize()
    {
        parent::initialize();
        $this->categoryHelper = new \Ixosoftware\Cms\Helpers\Category();
    }

    /**
     * Home page is not as simple as newest articles - all articles on home page are manually set by editor
     * and all saved into HomePage table as JSON
     *
     * This function reads current active json HomePage from database
     * then parsing the json to array and set to view
     */
    public function indexAction()
    {
        $homePage = HomePage::query()
            ->where('active = 1')
            ->limit(1)
            ->execute();
        if (empty($homePage)) {
            throw new Exception("Có lỗi không mong muốn xảy ra.");
            return false;
        }

        $activeRecord = $homePage->getFirst();

        // Set created and lastEdited properties
        $this->created = $activeRecord->created;
        $this->lastEdited = $activeRecord->lastEdited;

        $articleIds = [];
        $imageIds = [];
        $categoryIds = [];
        $boxIds = [];
        $eventIds = [];
        $pollIds = [];
        $articleWithJournalistIds = [];

        $homePage = json_decode($activeRecord->content);

        /**
         * Extract content from json
         */
        foreach ($homePage as $row) {
            // Get all boxes ID
            if (!empty($row->Box)) {
                foreach ($row->Box as $box) {
                    $boxIds[] = $box->ID;
                }
            }
        }
        $boxIds = array_unique($boxIds);

        /**
         * Select boxes information, then extract all articleIds, categoryIds and imageIds
         */
        $this->loadBoxByIds($boxIds, $categoryIds, $articleIds, $eventIds, $pollIds, $articleWithJournalistIds);

        /**
         * All boxes which does NOT find in database, it must be a custom box
         */
        $customBoxIds = [];
        foreach ($boxIds as $boxId) {
            if (!isset($this->boxesMap[$boxId])) {
                $customBoxIds[] = $boxId;
            }
        }

        $customBoxTypes = [];
        if (!empty($customBoxIds)) {
            foreach ($homePage as $row) {
                if (!empty($row->Box)) {
                    foreach ($row->Box as $box) {
                        if (in_array($box->ID, $customBoxIds)) {
                            if (empty($box->Event) && empty($box->Poll)) {
                                $customBoxTypes[] = $box->Type;
                            }
                        }
                    }
                }
            }
        }
        $customBoxTypes = array_unique($customBoxTypes);
        foreach ($customBoxTypes as $boxType) {
            $this->boxTypesArticlesMap[$boxType] = $this->helpers['complexSearchHelper']->loadArticleIdsByBoxType($boxType);
            if (!empty($this->boxTypesArticlesMap[$boxType])) {
                $articleIds = array_merge($articleIds, $this->boxTypesArticlesMap[$boxType]);
            }
        }

        /**
         * Load events article IDs
         */
        $eventIds = array_unique($eventIds);
        $this->loadEventArticleIds($eventIds, $imageIds);
        if (!empty($this->eventsMap)) {
            foreach ($this->eventsMap as $eventId => $event) {
                $articleIds = array_merge($articleIds, $event['articleIds']);
            }
        }

        /**
         * Load polls by IDs
         */
        $this->loadPollsByIds($pollIds);

        /**
         * Make list of ids unique
         */
        $articleIds = array_unique($articleIds);
        $categoryIds = array_unique($categoryIds);

        /**
         * Select all articles, categories, events and images from database by using only 4 queries
         */
        $journalistIds = [];
        $this->loadArticleByIds($articleIds, $imageIds, $articleWithJournalistIds, $journalistIds);
        $this->loadCategoryByIds($categoryIds);
        $this->loadJournalists($journalistIds, $imageIds);

        $imageIds = array_unique($imageIds);
        $this->loadImageByIds($imageIds);

        /**
         * Create rows and boxes data
         */
        $this->view->setVar('homeData', $this->formatBoxesData($homePage));
    }

    private function formatBoxesData($homeData = [])
    {
        $data = [];

        foreach ($homeData as $rowItem) {
            $rowBoxes = $rowItem->Box;

            $row = [
                'ID' => $rowItem->ID,
                'Title' => $rowItem->Title,
                'Type' => $rowItem->Type,
                'DataID' => $rowItem->DataID,
                'IsLayoutSide' => $rowItem->IsLayoutSide,
                'Box' => []
            ];

            foreach ($rowBoxes as $boxItem) {
                $boxId = $boxItem->ID;
                if (isset($this->boxesMap[$boxId])) {
                    $_tmpBox = $this->boxesMap[$boxId]->json;

                    $box = [
                        'ID' => $_tmpBox->ID,
                        'Title' => $_tmpBox->Title,
                        'Type' => $_tmpBox->Type,
                        'Category' => (!empty($_tmpBox->Category) && isset($this->categoriesMap[$_tmpBox->Category])) ? (array)$this->categoriesMap[$_tmpBox->Category] : null,
                        'Content' => !empty($_tmpBox->Content) ? $_tmpBox->Content : null,
                        'IsLayoutSide' => !empty($_tmpBox->IsLayoutSide) ? $_tmpBox->IsLayoutSide : 0,
                        'Navigation' => [],
                        'Posts' => []
                    ];

                    // Create navigation items
                    if (!empty($_tmpBox->Navigation) && !empty($_tmpBox->Navigation->Categorys)) {
                        foreach ($_tmpBox->Navigation->Categorys as $category) {
                            if (isset($this->categoriesMap[$category->ID])) {
                                $box['Navigation'][] = (array) $this->categoriesMap[$category->ID];
                            }
                        }
                    }

                    // Create posts items
                    if (!empty($_tmpBox->Posts)) {
                        foreach ($_tmpBox->Posts as $post) {
                            $postId = $post->ID;
                            if (isset($this->articlesMap[$postId])) {
                                $postData = $this->articlesMap[$postId]->toArray();
                                $postData['slug'] = Scaffolding::getArticleSlug($postData['metaSlug'], $postData['id']);

                                $articleImageId = $this->articlesMap[$postId]->imageId;

                                // If image is still available, use it
                                if (isset($this->imagesMap[$articleImageId])) {
                                    $postData['imageUrl'] = $this->imagesMap[$articleImageId]['fileName'];
                                } // Otherwise, use default image
                                else {
                                    $postData['imageUrl'] = $this->getDI()->getShared('config')->defaults->articleImageUrl;
                                }

                                // Load journalist info for special articles
                                if (!empty($post->showPostPreview)) {
                                    $journalistId = $postData['journalistId'];
                                    if (isset($journalistId)) {
                                        $avatarUrl = $this->config->defaults->authorAvatarUrl;
                                        if (isset($this->imagesMap[$this->journalistsMap[$journalistId]['avatarId']])) {
                                            $avatarUrl = $this->imagesMap[$this->journalistsMap[$journalistId]['avatarId']]['fileName'];
                                        }
                                        $postData['journalist'] = [
                                            'name' => $this->journalistsMap[$journalistId]['firstName'],
                                            'avatarUrl' => $avatarUrl
                                        ];
                                    }
                                }

                                $box['Posts'][] = $postData;
                            }
                        }
                    }

                    // Load event posts
                    if (!empty($_tmpBox->Event) && isset($this->eventsMap[$_tmpBox->Event])) {
                        $event = $this->eventsMap[$_tmpBox->Event];
                        $box['Event'] = $event['event'];
                        $box['Event']['imageUrl'] = $this->imagesMap[$event['event']['imageId']]['fileName'];
                        $box['Posts'] = $this->populateCustomBoxArticles($event['articleIds']);
                    }

                    // Load poll info
                    if (!empty($_tmpBox->Poll) && isset($this->pollsMap[$_tmpBox->Poll])) {
                        $box['Poll'] = $this->pollsMap[$_tmpBox->Poll];
                    }

                    $row['Box'][] = $box;
                }
                else {
                    $box = [
                        'ID' => $boxItem->ID,
                        'Title' => $boxItem->Title,
                        'Type' => $boxItem->Type,
                        'Category' => (!empty($boxItem->Category) && isset($this->categoriesMap[$boxItem->Category])) ? (array) $this->categoriesMap[$boxItem->Category] : null,
                        'Content' => !empty($boxItem->Content) ? $boxItem->Content : null,
                        'IsLayoutSide' => !empty($boxItem->IsLayoutSide) ? $boxItem->IsLayoutSide : 0,
                        'Navigation' => [],
                        'Posts' => []
                    ];

                    // Load posts for custom box type
                    if (isset($this->boxTypesArticlesMap[$boxItem->Type])) {
                        $box['Posts'] = $this->populateCustomBoxArticles($this->boxTypesArticlesMap[$boxItem->Type]);
                    }

                    $row['Box'][] = $box;
                }
            }

            $data[] = $row;
        }

        return $data;
    }

    /**
     * Load all home page box by IDs
     *
     * @return array - Associated array with key is box ID
     */
    private function loadBoxByIds($boxIds = [], &$categoryIds = [], &$articleIds = [], &$eventIds = [], &$pollIds = [], &$articleWithJournalistIds = []) {
        if (!empty($boxIds)) {
            $boxRows = HomePageBoxData::query()
                ->where('status = \'Active\'')
                ->inWhere('boxId', $boxIds)
                ->execute();

            if ($boxRows->count() > 0) {
                foreach ($boxRows as $row) {
                    if (!empty($row->json)) {
                        $row->json = json_decode($row->json);

                        // Get all categories ID from navigation
                        if (!empty($row->json->Navigation)) {
                            $navigation = $row->json->Navigation;

                            // Main navigation category if it is not a text-label
                            if (!empty($navigation->ID)) {
                                $categoryIds[] = $navigation->ID;
                            }

                            // Sub categories
                            if (!empty($navigation->Categorys)) {
                                foreach ($navigation->Categorys as $cat) {
                                    $categoryIds[] = $cat->ID;
                                }
                            }
                        }

                        // Get category for the box
                        if (!empty($row->json->Category)) {
                            $categoryIds[] = $row->json->Category;
                        }

                        // Get all posts ID
                        if (!empty($row->json->Posts)) {
                            foreach ($row->json->Posts as $idx => $post) {
                                // Get author information for first post of this box
                                if ($idx == 0 && $row->json->Type == 'Boxnews') {
                                    $articleWithJournalistIds[] = $post->ID;
                                    $post->showPostPreview = true;
                                }
                                $articleIds[] = $post->ID;
                            }
                        }

                        // Get events ID
                        if (!empty($row->json->Event)) {
                            $eventIds[] = $row->json->Event;
                        }

                        // Get polls ID
                        if (!empty($row->json->Poll)) {
                            $pollIds[] = $row->json->Poll;
                        }
                    }

                    $this->boxesMap[$row->boxId] = $row;
                }
            }
        }
    }

    /**
     * Load all articles by IDs
     *
     * @return array - Associated array with key is article id
     */
    private function loadArticleByIds($articleIds = [], &$imageIds, &$articleWithJournalistIds = [], &$journalistIds = [])
    {
        if (!empty($articleIds)) {
            $result = Article::query()
                ->columns('id, metaSlug, title, metaTitle, sapo, imageId, allowComment, commentCount, contentType, showJournalist, journalistId')
                ->where('status = \'Publish\'')
                ->inWhere('id', $articleIds)
                ->execute();

            foreach ($result as $article) {
                $article->slug = Scaffolding::getArticleSlug($article->metaSlug, $article->id);
                $this->articlesMap[$article->id] = $article;

                if (!empty($article->imageId)) {
                    $imageIds[] = $article->imageId;
                }

                if (in_array($article->id, $articleWithJournalistIds)) {
                    $journalistIds[] = $article->journalistId;
                }
            }
        }
    }

    /**
     * Load all categories by IDs
     *
     * @return array - Associated array with key is category id
     */
    private function loadCategoryByIds($categoryIds = [])
    {
        $this->categoriesMap = $this->categoryHelper->getAllCategories();
    }

    /**
     * Load all images by IDs
     *
     * @return array - Associated array with key is image id
     */
    private function loadImageByIds($imageIds = [])
    {
        if (!empty($imageIds)) {
            $result = File::query()
                ->inWhere('id', $imageIds)
                ->execute();
            foreach ($result as $file) {
                $this->imagesMap[$file->id] = $file->toArray();
            }
        }
    }

    /**
     * Using Elasticsearch to query list of articles
     *
     * @return array - List of article IDs for each event
     */
    private function loadEventArticleIds($eventIds = [], &$imageIds = [])
    {
        $result = Event::query()
            ->columns('id,title,description,metaSlug,metaTitle,imageId')
            ->where('status = \'Active\'')
            ->inWhere('id', $eventIds)
            ->execute();
        if ($result->count()) {
            foreach ($result as $event) {
                $event->slug = Scaffolding::getEventSlug($event->id, $event->metaSlug);
                $ids = $this->helpers['complexSearchHelper']->getTopArticleIdsFromEvent($event->id, [], 0, 5);

                $this->eventsMap[$event->id] = [
                    'event' => $event->toArray(),
                    'articleIds' => empty($ids) ? [] : $ids
                ];

                $imageIds[] = $event->imageId;
            }
        }
    }

    /**
     * Load polls
     */
    private function loadPollsByIds($pollIds = [])
    {
        if (!empty($pollIds)) {
            foreach ($pollIds as $pollId) {
                $this->pollsMap[$pollId] = Memcached::getPollById($pollId);
            }
        }
    }

    /**
     * Load journalists by IDs
     */
    private function loadJournalists($journalistIds = [], &$imageIds = [])
    {
        if (!empty($journalistIds)) {
            $result = Member::query()
                ->columns('id, firstName, avatarId')
                ->inWhere('id', $journalistIds)
                ->execute();
            foreach ($result as $member) {
                if (!empty($member->avatarId)) {
                    $imageIds[] = $member->avatarId;
                }
                $this->journalistsMap[$member->id] = $member->toArray();
            }
        }
    }

    /**
     * Populate box posts
     *
     * @return array - List of articles
     */
    private function populateCustomBoxArticles($articleIds = [])
    {
        $articles = [];

        foreach ($articleIds as $articleId) {
            if (isset($this->articlesMap[$articleId])) {
                $article = $this->articlesMap[$articleId]->toArray();

                if ($this->imagesMap[$article['imageId']]) {
                    $article['imageUrl'] = $this->imagesMap[$article['imageId']]['fileName'];
                }
                else {
                    $article['imageUrl'] = $this->getDI()->getShared('config')->defaults->articleImageUrl;
                }

                $articles[] = $article;
            }
        }

        return $articles;
    }

    public function metaDataAction()
    {
        $this->view->disable();

        $columns = [
            'ID'            => 'id',
            'ClassName'     => 'className',
            'LastEdited'    => 'lastEdited',
            'Created'       => 'created',
            'FullName'      => 'fullName',
            'Email'         => 'email',
            'Phone'         => 'phone',
            'IP'            => 'phone',
            'User-Agent'    => 'userAgent',
            'Referer'       => 'referer'
        ];
        foreach ($columns as $key => $value) {
            echo '\''.$key.'\', ';
        }
        echo '<br />';

        foreach ($columns as $key => $value) {
            echo 'public $'.$value.';<br/>';
        }

        die();
    }

    public function setMetaTags()
    {
        // Foreach default meta tags, if one of them set in $homeConfig then use the config value
        // otherwise use default
        $defaultMetaTags = $this->config->defaults->homePageMetaTags;
        $homeConfig = $this->globalConfigs['HomeMeta'];
        foreach ($defaultMetaTags as $tagName => $tagValue) {
            if (isset($homeConfig[$tagName])) {
                $this->metaTags[$tagName] = $homeConfig[$tagName];
            }
            else {
                $this->metaTags[$tagName] = $tagValue;
            }
        }

        $this->metaTags['Alternates'] = [
            'handheld' => $this->helpers['urlHelper']->getMobileDomain()
        ];

        $this->metaTags['MetaSlug'] = ($this->isMobile ? $this->helpers['urlHelper']->getMobileDomain() : $this->helpers['urlHelper']->getDesktopDomain());
        $this->metaTags['PublishTime'] = date('c', strtotime($this->created));
        $this->metaTags['LastEdited'] = date('c', strtotime($this->lastEdited));
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}

