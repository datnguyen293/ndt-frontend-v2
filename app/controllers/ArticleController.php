<?php
namespace Ixosoftware\Cms\Controllers;

use Endroid\QrCode\QrCode;
use Ixosoftware\Cms\Helpers\Captcha as CaptchaHelper;
use Ixosoftware\Cms\Helpers\Category as CategoryHelper;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Helpers\ShortcodeParser;
use Ixosoftware\Cms\Models\Event;
use Ixosoftware\Cms\Models\Member;
use Ixosoftware\Cms\Models\Tag;
use Ixosoftware\Cms\Plugins\simple_html_dom;
use Phalcon\Exception;

class ArticleController extends ControllerBase
{
    private $article;
    private $articleLink;
    private $articleAds;
    private $ampAds;
    private $categoryHelper;
    private $primaryCategory;
    private $captchaHelper;

    public function initialize()
    {
        parent::initialize();
        $this->categoryHelper = new CategoryHelper();
        $this->captchaHelper = new CaptchaHelper();
    }

    /**
     * Handle for request to an article
     * GET: /<slug>-a<id>.<html|amp>
     */
    public function indexAction()
    {
        $type = $this->dispatcher->getParam('type');

        /**
         * Set amp layout
         */
        if ($type == 'amp') {
            $this->setAmpLayout();
        }

        $articleId = (int)$this->dispatcher->getParam('articleId');
        $slug = $this->dispatcher->getParam('slug');

        if (empty($articleId)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Bài viết không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }

        $imageIds = [];
        $articleIds = [];
        $eventIds = [];

        /**
         * I prefer to use raw-query here to avoid extra joins generated by Phalcon's models
         * As the database size is very big (more than 10GB and still increasing)
         * so any single query would be evaluated carefully to improve the speed
         * Database index would be double-checked also
         */
        $sql = 'SELECT a.ID as id, a.Title as title, a.Description as description, a.MetaSlug as metaSlug, a.MetaTitle as metaTitle, a.MetaKeyword as metaKeyword, a.JsonContent as jsonContent, ' .
            'a.ImageId as imageId, a.LastEdited as lastEdited, a.PollID as pollId, a.Sapo as sapo, a.PrimaryCategoryID as primaryCategoryId, a.Content as content, ' .
            'a.EventIDs as eventIds, a.TagIDs as tagIds, a.CategoryIDs as categoryIds, a.PublishTime as publishTime, a.LiveUpdateTime as liveUpdateTime, ' .
            'a.journalistID as journalistId, a.ShowJournalist as showJournalist, a.ShowComment as showComment, ' .
            'a.CommentCount as commentCount, a.ContentType as contentType, a.SourceID as sourceId, f.Filename as imageUrl ' .
            'FROM Article a ' .
            'LEFT JOIN File f ON f.ID = a.ImageID ' .
            'WHERE a.id = :id AND a.`status` = :status AND a.stage = :stage ' .
            'LIMIT 1';
        $this->article = $this->getDI()->getShared('db')->fetchOne($sql, \Phalcon\Db::FETCH_ASSOC, [
            'id' => $articleId,
            'status' => 'Publish',
            'stage' => 'Publish'
        ]);

        if (empty($this->article)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Bài viết không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }

        if ($slug != $this->article['metaSlug']) {
            return $this->response->redirect(Scaffolding::getArticleSlug($this->article['metaSlug'], $articleId));
        }

        // Actual link
        $uri = Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id']);
        $this->articleLink = $this->helpers['urlHelper']->getDesktopUrl($uri);

        $this->article['slug'] = Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id']);
        $this->article['formattedPublishTime'] = date('h:m A', strtotime($this->article['publishTime']));

        /**
         * Generate QR code from article link and send the image to the response
         */
        if (!empty($this->request->getQuery('qr'))) {
            $qrCode = new QrCode($this->articleLink);
            $qrCode->setSize(500)
                ->setWriterByName('png')
                ->setMargin(10)
                ->setEncoding('UTF-8')
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255])
                ->setValidateResult(false);
            $this->response->setHeader('Content-Type', $qrCode->getContentType());
            $this->response->setContent($qrCode->writeString());
            $this->response->send();
            return false;
        }

        /**
         * Load primary category
         */
        $this->primaryCategory = $this->categoryHelper->getCategoryById($this->article['primaryCategoryId']);
        $rootCategory = $this->categoryHelper->getRootCategory($this->primaryCategory);
        if (!empty($rootCategory->pinedEventId)) {
            $eventIds[] = $rootCategory->pinedEventId;
        }

        /**
         * TODO: Make a smarter way to set up e-magazine instead of checking fixed article IDs in configuration
         */
        $emagazineArticleIds = $this->config->emagazine->articleIds->toArray();
        if (!empty($emagazineArticleIds) && in_array($articleId, $emagazineArticleIds)) {
            $this->setEMagazineLayout();
        }

        /**
         * Load events for current article
         */
        $articleEventIds = [];
        $events = [];
        if (!empty($this->article['eventIds'])) {
            $articleEventIds = json_decode($this->article['eventIds']);
            if (!empty($articleEventIds)) {
                $eventIds = array_merge($eventIds, $articleEventIds);
            }
        }
        if (!empty($eventIds)) {
            $events = $this->getEventsByIds($eventIds, $imageIds);
        }

        /**
         * Load tags for current article
         */
        $tags = [];
        if (!empty($this->article['tagIds'])) {
            $tagIds = json_decode($this->article['tagIds']);
            if (!empty($tagIds) && is_array($tagIds)) {
                $tags = $this->getTagsByIds($tagIds);
            }
        }
        $this->article['tags'] = $tags;

        /**
         * Load categories for current article
         */
        $articleCategories = [];
        if (!empty($this->article['categoryIds'])) {
            $categoryIds = json_decode($this->article['categoryIds']);
            if (!empty($categoryIds) && is_array($categoryIds)) {
                foreach ($this->categoryHelper->getAllCategories() as $categoryId => $category) {
                    if (in_array($categoryId, $categoryIds)) {
                        $articleCategories[$categoryId] = $category;
                    }
                }
            }
        }
        $this->article['categories'] = $articleCategories;

        /**
         * Load photos for articles
         */
        if (strtolower($this->article['contentType']) == 'photo') {
            $this->article['Photos'] = [];
            $jsonObjs = json_decode($this->article['jsonContent'], true);
            if (json_last_error() == JSON_ERROR_NONE && !empty($jsonObjs)) {
                foreach ($jsonObjs as $photo) {
                    $this->article['Photos'][] = $photo['photo']['ID'];
                }
            }
            if (!empty($this->article['Photos'])) {
                $imageIds = array_merge($imageIds, $this->article['Photos']);
            }
        }

        /**
         * Format featured image
         */
        if (empty($this->article['imageUrl'])) {
            $this->article['imageUrl'] = $this->config->defaults->articleImageUrl;
        } else {
            $this->article['imageUrl'] = $this->helpers['imageHelper']->generateThumbLink($this->article['imageUrl'], 600, 600);
        }

        /**
         * Event stream
         */
        $eventArticleIds = [];
        if (!empty($articleEventIds)) {
            $lastEventId = array_slice($articleEventIds, -1)[0];
            $eventArticleIds = $this->helpers['complexSearchHelper']->getTopArticleIdsFromEvent($lastEventId, [$articleId], 0, 5);
            $articleIds = array_merge($articleIds, $eventArticleIds);
        }

        /**
         * Follow an event
         */
        $theoDongSuKienArticleIds = [];
        if (!empty($rootCategory->pinedEventId)) {
            if (isset($events[$rootCategory->pinedEventId])) {
                $theoDongSuKienArticleIds = $this->helpers['complexSearchHelper']->getTopArticleIdsFromEvent($rootCategory->pinedEventId, [], 0, 5);
                $articleIds = array_merge($articleIds, $theoDongSuKienArticleIds);
            }
        }

        /**
         * Load article journalist
         */
        $journalist = Member::query()
            ->columns('id, firstName, avatarId, penName')
            ->where('id', $this->article['journalistId'])
            ->execute()
            ->getFirst()
            ->toArray();

        /**
         * Load articles and images from IDs
         */
        $articleIds = array_unique($articleIds);
        $articlesMap = $this->getArticlesByIds($articleIds, $imageIds);

        $imageIds = array_unique($imageIds);
        $this->getImagesByIds($imageIds);

        /**
         * Make article photos a list of objects
         */
        if (!empty($this->article['Photos'])) {
            $photos = [];
            foreach ($this->article['Photos'] as $photoId) {

            }
        }

        /**
         * Create article events
         */
        $articleEvents = [];
        if (!empty($this->article['eventIds'])) {
            $ids = array_merge($eventIds, json_decode($this->article['eventIds']));
            if (!empty($eventIds) && is_array($eventIds)) {
                foreach ($ids as $eventId) {
                    $event = $events[$eventId];
                    $event['imageUrl'] = $this->helpers['imageHelper']->getCustomDesktopSize($imagesMap[$event['imageId']]['fileName'], 'squad');
                    $articleEvents[] = $event;
                }
            }
        }
        $this->article['events'] = $articleEvents;

        /**
         * Custom box
         */
        $dongSuKienArticles = $this->extractArticlesFromMapByIds($eventArticleIds);
        $theoDongSuKienArticles = $this->extractArticlesFromMapByIds($theoDongSuKienArticleIds);

        /**
         * Load article ads
         */
        $this->loadArticleAds();

        /**
         * Customize article content
         */
        $this->article['content'] = $this->formatContent($this->article['content'], $type == 'amp' ? 'amp' : ($this->isMobile ? 'mobile' : 'desktop'));

        $this->view->setVars([
            'article' => $this->article,
            'journalist' => $journalist,
            'primaryCategory' => $this->primaryCategory,
            'dongSuKien' => $dongSuKienArticles,
            'theoDongSuKien' => $theoDongSuKienArticles
        ]);
    }

    /**
     * Depending on what type of layout to be displayed, we have to modified the content of article a little
     * to make it best fit to the screen.
     * We also have to insert advertisement into the content
     *
     * AMP is very special so content must be strictly followed Google's guide
     *
     * @param $content - Article's content
     * @param $type - Layout to display, default is 'desktop'
     * @return string - Formatted content to display
     */
    private function formatContent($content, $type = 'desktop')
    {
        // Parse images
        $content = $this->processImagesInContent($content, $type);

        // Remove 3rd party iframe
        $content = $this->processIframeInContent($content);

        // Xử lý tìm thẻ P sau đó replace anch text
        $content = $this->processTagAnchorText($content);

        // Xử lý nội dung amp
        if ($type == 'amp') {
            $content = $this->formatContentForAmp($content);
        }
        else if ($type == 'mobile') {
            $content = $this->formatContentForMobile($content);
        }

        if ($this->hasImageSlider($content) && in_array($type, ['desktop', 'mobile'])) {
            $content = $this->formatImageSlider($content, $type);
        }

        return $content;
    }

    /**
     * Format content for AMP following requirement from Google
     *
     * @param $content - Article's content
     * @return string - Formatted content to display in AMP view
     */
    private function formatContentForAmp($content)
    {
        $content = $this->ampConvertImage($content);
        $content = $this->ampRemoveInlineStyle($content);
        $content = $this->ampRemoveInlineScript($content);
        $content = $this->ampProcessIframe($content);
        $content = $this->ampProcessVideo($content);
        $content = $this->ampAddAds($content);

        return $content;
    }

    /**
     * Format content for mobile layout
     *
     * @param $content
     * @return string - Formatted content to display in mobile view
     */
    private function formatContentForMobile($content)
    {
        $content = $this->mobileProcessAds($content);

        return $content;
    }

    /**
     * Create image slider for an article
     *
     * @param $content
     * @param string $type
     * @return mixed
     */
    private function formatImageSlider($content, $type = 'desktop')
    {
        $re = '/<hr\sclass=\"ndtPhotoHere\"\s\/>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $slider = $this->renderSliderImages($type);

                $content = str_replace($match, $slider, $content);
            }
        }
        return $content;
    }

    private function processImagesInContent($content, $type = 'desktop')
    {
        $imageSize = $this->getImageSize($type);

        $re = '/<img\s[^>]*?src\s*=\s*[\'\"]([^\'\"]*?)[\'\"][^>]*?>/';
        preg_match_all($re, $content, $matches);

        if (count($matches) > 0) {

            // Replace images with proper link with size
            foreach ($matches[1] as $match) {
                if ((strpos($match, 'http') === 0) && (strpos($match, 'nguoiduatin.vn') !== false)) {
                    $newLink = $this->genLinkImage($match, $imageSize['width'], $imageSize['height'], $type);
                    $newLink = str_replace(' ', '%20', $newLink);
                    $content = str_replace($match, $newLink, $content);
                }
            }

            // Remove width, height, alt and title from images
            foreach ($matches[0] as $index => $match) {
                if (strpos($match, '<img') === 0) {
                    $re = [];
                    // Xoá các tag width, height, alt, title
                    $re[] = '/width\s?=\s?(\'\d+\'|\"\d+\")/';
                    $re[] = '/height\s?=\s?(\'\d+\'|\"\d+\")/';
                    $re[] = '/alt\s*=\s*([\'"]).*?\1/';
                    $re[] = '/title\s*=\s*([\'"]).*?\1/';
                    $re[] = '/border\s*=\s*([\'"]).*?\1/';
                    if ($index > 0) {
                        $alt = $this->imageAlt() . ' (Hình ' . ($index + 1) . ').';
                    } else {
                        $alt = $this->imageAlt();
                    }
                    $newIMG = preg_replace($re, ['', '', 'alt="' . $alt . '"', '', ''], $match);
                    $content = str_replace($match, $newIMG, $content);
                }
            }
        }

        return $content;
    }

    /**
     * Remove non-nguoiduatin.vn iframe
     * If link of iframe is not nguoiduatin.vn, remove iframe
     */
    private function processIframeInContent($content)
    {
        $re = '/<\s*iframe\X*?iframe\s*>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $url = $this->getSrcValueFromString($match);
                if ($this->isNguoiduatinUrl($url)) {
                    $content = str_replace($match, '', $content);
                }
            }
        }
        return $content;
    }

    private function processTagAnchorText($content, $type = 'desktop')
    {
        $tagsWithAnchorText = [];
        $tags = $this->article['tags'];

        if (empty($tags)) {
            return $content;
        }

        foreach ($tags as $tag) {
            if (!empty($tag->anchorText) && $tag->anchorTextActive) {
                $tagsWithAnchorText[] = $tag;
            }
        }

        if (empty($tagsWithAnchorText)) {
            return $content;
        }

        $articleUri = Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id']);
        $articleUriAmp = Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id'], 'amp');
        $articleFullUrl = $this->helpers['urlHelper']->getDesktopUrl($articleUri);
        $urlHelper = $this->helpers['urlHelper'];

        // Find all texts in <p> tags
        $re = '/<\s*(p*)\b[^>]*>(.*?)<\/\1>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            $sortedKeys = [];
            for ($i = 0, $len = count($matches[0]); $i < $len; $i++) {
                $sortedKeys[$i] = 0;
            }

            foreach ($tagsWithAnchorText as $tag) {
                $findMe = '/' . str_replace([' ', '/', '.'], ['\s', '\/', '\.'], trim($tag->anchorText)) . '/i';

                // Ưu tiên các block chưa chứa anchor text đã được thay thế => phân tán link ra các block khác nhau
                asort($sortedKeys);

                // foreach ($matches[0] as $idx => $match) {
                foreach ($sortedKeys as $key => $replacedCount) {
                    $match = $matches[0][$key];
                    preg_match($findMe, $match, $subMatches);
                    if (!empty($subMatches)) {
                        $anchorText = mb_substr($match, mb_stripos($match, $tag->anchorText), mb_strlen($tag->anchorText));
                        $replaceWith = '<a href="' . $urlHelper->getFullUrl(Scaffolding::getTagSlug($tag->metaSlug), $type) . '">' . $anchorText . '</a>';
                        $result = preg_replace($findMe, $replaceWith, $match, 1);
                        $matches[0][$key] = $result;
                        $content = str_replace($match, $result, $content);

                        $sortedKeys[$key] += 1;

                        break;
                    }
                }
            }
        }

        return $content;
    }

    /**
     * Convert all <img> to <amp-img>
     */
    private function ampConvertImage($content)
    {
        $re = '/<img\s[^>]*?src\s*=\s*[\'\"]([^\'\"]*?)[\'\"][^>]*?>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                if (strpos($match, '<img') === 0) {
                    $dataSrc = str_replace('<img', '<amp-img width="1120" height="832" layout=responsive', $match);
                    if (strpos($match, '/>')) {
                        $dataSrc = str_replace('/>', '></amp-img>', $dataSrc);
                    }
                    else {
                        $dataSrc = str_replace('>', '></amp-img>', $dataSrc);
                    }
                    $content = str_replace($match, $dataSrc, $content);
                }
            }
        }
        return $content;
    }

    /**
     * Remove inline style
     */
    private function ampRemoveInlineStyle($content)
    {
        $re = '/style\s*=\s*([\'"]).*?\1/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $content = str_replace($match, ' ', $content);
            }
        }
        return $content;
    }

    /**
     * Remove all <script> in html content for AMP page
     */
    private function ampRemoveInlineScript($content)
    {
        $re = '/<script\s*\X*<\/script>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $content = str_replace($match, $this->ampNotSupportedLink($this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id'], 'amp'))), $content);
            }
        }
        return $content;
    }

    private function ampNotSupportedLink($link)
    {
        return ' <div class="box"> <a href="' . $link . '">Nhấn vào đây để xem chi tiết</a></div>';
    }

    private function ampProcessIframe($content)
    {
        $re = '/<\s*iframe\X*?iframe\s*>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $url = $this->getSrcValueFromString($match);
                if ($this->isYoutube($url)) {
                    $id = $this->getYoutubeId($url);
                    $content = str_replace($match, $this->ampGenYoutubeLink($id), $content);
                } else if ($this->isHttpsSrc($url)) {
                    $this->genIframe($url);
                    $content = str_replace($match, $this->genIframe($url), $content);

                } else if ($this->isNguoiduatinUrl($url)) {
                    $content = str_replace($match, '', $content);
                } else {
                    $content = str_replace($match, $this->ampNotSupportedLink($this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id']))), $content);
                }
            }
        }
        return $content;
    }

    private function ampProcessVideo($content)
    {
        $re = '/<\s*video\X*?video\s*>/';
        preg_match_all($re, $content, $matches);
        if (count($matches) > 0) {
            foreach ($matches[0] as $match) {
                $url = $this->getSrcValueFromString($match);
                if ($this->isHttpsSrc($url)) {
                    $content = str_replace($match, $this->ampGenVideoLink($url), $content);
                } else {
                    $content = str_replace($match, $this->ampNotSupportedLink($this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id']))), $content);
                }
            }
        }
        return $content;
    }

    private function ampGenVideoLink($src)
    {
        return '<amp-video 
                controls
                width="640" height="360"
                layout="responsive" >
                    <source
                        src="' . $src . '"
                        type="video/mp4" />
                    <div fallback>
                        <p>Trình duyệt của bạn không hỗ trợ trình phát video.</p>
                    </div>
                </amp-video>';
    }

    private function ampGenYoutubeLink($id)
    {
        return '<amp-youtube
                    data-videoid="' . $id . '" 
                    layout="responsive" 
                    width="480" height="270">
                </amp-youtube>';
    }

    private function ampAddAds($content)
    {
        if (empty($this->ampAds) || empty($this->ampAds['AMP_Flying_Carpet'])) {
            return $content;
        }

        $ampAdsBefore = '<div class="clearfix"></div>
            <amp-fx-flying-carpet height="550px">
            <div class="amp-ad">';

        $ampAdsAfter = '
            </div>
            </amp-fx-flying-carpet>
            <div class="clearfix"></div>
            ';

        $middleAds = $this->ampAds['AMP_Flying_Carpet'];
        $middleAds = $ampAdsBefore . $middleAds . $ampAdsAfter;

        return $this->addAdsToMiddleOfContent($content, $middleAds);
    }

    private function mobileProcessAds($content)
    {
        if (empty($this->articleAds) || empty($this->articleAds['M_PostMid'])) {
            return $content;
        }
        $middleAds = $this->articleAds['M_PostMid'];

        return $this->addAdsToMiddleOfContent($content, $middleAds);
    }

    private function addAdsToMiddleOfContent($content, $ads = '')
    {
        try {
            $dom = new simple_html_dom($content);
            $elements = $dom->find('p>*');
            $numChildNodes = count($elements);
            if ($numChildNodes < 5) {
                return $content;
            }

            $midChild = $elements[ceil($numChildNodes / 2)];
            $midChild->outertext = $ads . $midChild->outertext;

            return $dom;
        }
        catch (Exception $e) {
            return $content;
        }
    }

    private function getSrcValueFromString($str)
    {
        $re = '/src\s*=\s*([\'\"])([^\'\"]*?)\1/';
        preg_match($re, $str, $matches);
        if (count($matches) > 0) {
            // Lấy bên trong src=""
            return $matches[2];
        }
        return '';
    }

    private function genIframe($src)
    {
        return '<amp-iframe
                    width="500" height="281"
                    layout="responsive"
                    sandbox="allow-scripts allow-same-origin allow-popups"
                    allowfullscreen
                    frameborder="0"
                    src="' . $src . '"> 
               </amp-iframe>';
    }

    private function isNguoiduatinUrl($url)
    {
        if (strpos($url, 'www.nguoiduatin.vn') !== false) {
            return true;
        } else return false;
    }

    private function isHttpsSrc($url)
    {
        if (strpos($url, 'https') !== false) {
            return true;
        } else return false;
    }

    private function isYoutube($url)
    {
        if (strpos($url, 'youtube') !== false) {
            return true;
        } else return false;
    }

    private function getYoutubeId($url)
    {
        $re = '/embed\/([^\#\?\"]+)/';
        preg_match($re, $url, $matches);
        if (count($matches) > 0) {
            // Lấy các ký tự sau embed/
            return $matches[1];
        }
    }

    private function getImageSize($type = 'desktop')
    {
        switch ($type) {
            case 'desktop':
                return ['width' => 0, 'height' => 0];

            case 'mobile':
                return ['width' => 1200, 'height' => 0];

            default:
                return ['width' => 1120, 'height' => 832];
        }
    }

    private function genLinkImage($url, $width = 0, $height = 0, $type = 'desktop')
    {
        if (strpos($url, 'http') === 0) {
            //Convert từ dữ liệu cũ thành link file gốc
            $re = '/thumb_x(\d*|\d+)x(\d*|\d+)\//';
            $str = $url;
            $str = preg_replace($re, "", $str);
            if ($width == 0) {
                return $str;
            }
            //Thêm size cần thiết cho link
            $re = '/.nguoiduatin.vn/';
            preg_match($re, $str, $matches);
            if (isset($matches[0])) {
                //Chi replace cai dau tien
                $from = $matches[0]; //nguoiduatin.vn
                if (!empty($type) && $type == 'amp') {
                    $to = $from . '/amp_thumb_x' . ($width ? $width : '') . 'x' . ($height ? $height : '');
                } else {
                    $to = $from . '/thumb_x' . ($width ? $width : '') . 'x' . ($height ? $height : '');
                }
                $from = '/' . preg_quote($from, '/') . '/';
                //end replace

                $imageUrl = preg_replace($from, $to, $str, 1);
            } else {
                $imageUrl = $str;
            }
            return $imageUrl;
        }
        return $url;
    }

    private function hasImageSlider($content)
    {
        $re = '/<hr\sclass=\"ndtPhotoHere\"\s\/>/';
        preg_match_all($re, $content, $matches);
        if (count($matches[0]) > 0) {
            return true;
        }
        return false;
    }

    private function renderSliderImages($type = 'desktop')
    {
        // TODO: Render image slider
    }

    private function imageAlt()
    {
        return str_replace('"', "'", $this->primaryCategory->title . ' - ' . $this->article['title']);
    }

    private function getEventsByIds($eventIds = [], &$imageIds = [])
    {
        $events = [];

        if (!empty($eventIds)) {
            $result = Event::query()
                ->columns('id,title,description,metaSlug,metaTitle,imageId')
                ->where('status = \'Active\'')
                ->inWhere('id', $eventIds)
                ->execute();
            if ($result->count()) {
                foreach ($result as $event) {
                    $event->slug = Scaffolding::getEventSlug($event->id, $event->metaSlug);

                    if (!empty($event->imageId)) {
                        $imageIds[] = $event->imageId;
                    }

                    $events[$event->id] = $event->toArray();
                }
            }
        }

        return $events;
    }

    private function getTagsByIds($tagIds = [])
    {
        $tags = [];

        if (!empty($tagIds)) {
            $tags = Tag::query()
                ->inWhere('id', $tagIds)
                ->execute();
        }

        return $tags;
    }

    private function setAmpLayout()
    {
        $this->view->setLayout('amp.layout');
    }

    private function setEMagazineLayout()
    {
        $this->view->setLayout('emagazine.layout');
    }

    private function loadArticleAds()
    {
        // Default ads is global configuration for all articles
        $articleAds = $this->globalConfigs['AdsArticle'];
        $ampAds = $this->globalConfigs['AdsAMP'];

        // If its category is set ads, use for all articles
        if (!empty($this->primaryCategory)) {
            if (!empty($this->primaryCategory->adsJson)) {
                $articleAds = json_decode($this->primaryCategory->adsJson);
            }
            else {
                $parentCategories = $this->categoryHelper->getParentCategories($this->primaryCategory);
                foreach ($parentCategories as $category) {
                    if (!empty($category->adsJson)) {
                        $articleAds = json_decode($category->adsJson);
                    }
                }
            }
        }

        $this->articleAds = $articleAds;
        $this->ampAds = $ampAds;
    }

    public function setMetaTags()
    {
        $this->metaTags['MetaTitle']        = empty($this->article['metaTitle']) ? $this->article['title'] : $this->article['metaTitle'];
        $this->metaTags['MetaDescription']  = empty($this->article['metaDescription']) ? $this->article['sapo'] : $this->article['metaDescription'];
        $this->metaTags['MetaKeyword']      = $this->article['metaKeyword'];
        $this->metaTags['MetaSlug']         = $this->articleLink;
        $this->metaTags['PageType']         = 'Article';
        $this->metaTags['PublishTime']      = $this->article['publishTime'];
        $this->metaTags['LastEdited']       = $this->article['lastEdited'];
        $this->metaTags['Image']            = $this->article['imageUrl'];
        $this->metaTags['Alternates']       = [
            'amphtml'  => $this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id'], 'amp')),
            'handheld' => $this->helpers['urlHelper']->getMobileUrl(Scaffolding::getArticleSlug($this->article['metaSlug'], $this->article['id'], 'html'))
        ];

        // Generate breadcrumb list
        $breadcrumbList = [];
        array_unshift($breadcrumbList, [
            'id'        => 0,
            'parentId'  => 0,
            'title'     => 'www.nguoiduatin.vn',
            'metaTitle' => 'www.nguoiduatin.vn',
            'metaSlug'  => $this->isMobile ? $this->helpers['urlHelper']->getMobileDomain() : $this->helpers['urlHelper']->getDesktopDomain()
        ]);

        $parentCategories = $this->categoryHelper->getParentCategories($this->primaryCategory);
        foreach ($parentCategories as $category) {
            $breadcrumbList[] = $this->categoryHelper->navCategoryData($category);
        }

        $this->metaTags['BreadcrumbList']    = $breadcrumbList;
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}