<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\Category as CategoryHelper;
use Ixosoftware\Cms\Helpers\Memcached;
use Ixosoftware\Cms\Helpers\NdtElasticSearch;
use Ixosoftware\Cms\Helpers\Pagination;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Helpers\StringUtils as StringHelper;
use Ixosoftware\Cms\Models\Event;

// We are using ElasticSearch to read list of articles, which is limited to no more 10k articles offset
define('MAX_ALLOW_ARTICLE_OFFSET', 9999);

class CategoryController extends ControllerBase
{
    private $categoryHelper;
    private $stringHelper;

    private $category;
    private $metaTitleTrail;
    private $lastEdited;

    private $data = [];
    
    public function initialize()
    {
        parent::initialize();
        $this->categoryHelper = new CategoryHelper();
        $this->stringHelper = new StringHelper();
    }

    /**
     * Handle category page
     * GET: /c/<slug>
     */
    public function indexAction()
    {
        $slug = $this->dispatcher->getParam('slug');
        $page = $this->dispatcher->getParam('page');
        if (!$page && $this->stringHelper->endsWith($this->router->getRewriteUri(), '/page')) {
            $this->response->redirect('/c/' . $slug);
            return false;
        }
        if ((int)$page <= 0) { $page = 1; }

        /**
         * Don't mess it up with very old articles by restricted the max offset
         * Also Elasticsearch does NOT allow to load more than 10k documents (by default)
         */
        $numArticlesPerCategoryPage = $this->config->defaults->numArticlesPerCategoryPage;
        $maxPageAllowed = ceil(MAX_ALLOW_ARTICLE_OFFSET / $numArticlesPerCategoryPage);
        if ($page > $maxPageAllowed) {
            $page = $maxPageAllowed;
        }

        $articleIds = [];
        $pinedArticleIds = [];
        $navCategoryIds = [];
        $imageIds = [];

        /**
         * Get category information & generate sub-navigation
         */
        $category = $this->categoryHelper->getCategoryBySlug($slug);
        if (empty($category)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Chuyên mục không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }
        $this->category = $category;
        $this->lastEdited = $this->category->lastEdited;

        /**
         * Load root category and generate its navigation (category tree)
         */
        $isRootCategory = empty($category->parentId);

        if ($isRootCategory) {
            $rootCategory = $category;

        }
        else {
            $rootCategory = $this->categoryHelper->getRootCategory($category);
        }

        $navCategoryIds = $this->categoryHelper->getSubcategoryIds($rootCategory);
        if (!$isRootCategory) {
            $subCategoryIds = $this->categoryHelper->getSubcategoryIds($category);
        }
        else {
            $subCategoryIds = $navCategoryIds;
        }
        $subCategoryIds[] = $category->id;

        // Navigation included root category
        array_unshift($navCategoryIds, $rootCategory->id);

        /**
         * Get pined articles on view
         * If no article pined, promote 3 newest article as pined
         */
        $pinedAllCategories = Memcached::getCategoryPinedArticles();
        if (isset($pinedAllCategories[$category->id])) {
            $pinedArticleIds = $pinedAllCategories[$category->id];
        }
        $getMore = 3 - count($pinedArticleIds);

        /**
         * Get articles for current page
         * For the first page, we don't want to query ElasticSearch again for pined articles
         * so we do some tricks with $numArticlesPerCategoryPage and $from
         */
        $from = ($page - 1) * $numArticlesPerCategoryPage;

        if ($page == 1 && $getMore > 0) {
            $numArticlesPerCategoryPage += $getMore;
        }
        if ($page > 1 && $getMore > 0) {
            $from += $getMore;
        }

        $serverConfig = $this->config->elasticSearch->toArray();
        $results = NdtElasticSearch::getInstance($serverConfig)->relatedArticlesInCategories($subCategoryIds, $pinedArticleIds, $numArticlesPerCategoryPage, $from);
        $totalArticles = (int) $results['total'];
        if ($totalArticles > MAX_ALLOW_ARTICLE_OFFSET) {
            $totalArticles = MAX_ALLOW_ARTICLE_OFFSET;
        }

        $pageArticleIds = $results['articleIds'];
        $articleIds = array_merge($articleIds, $pageArticleIds);

        // If no pined articles found, get latest articles from current category
        if ($getMore > 0) {
            if ($page == 1) {
                $pinedArticleIds = array_merge($pinedArticleIds, array_slice($pageArticleIds, 0, $getMore));
                $pageArticleIds = array_slice($pageArticleIds, $getMore);
            }
            else {
                $results = NdtElasticSearch::getInstance($serverConfig)->relatedArticlesInCategories($subCategoryIds, $pinedArticleIds, $getMore, 0);
                $pinedArticleIds = array_merge($pinedArticleIds, $results['articleIds']);
            }
        }

        // Make sure always 3 pin-articles
        $pinedArticleIds = array_slice($pinedArticleIds, 0, 3);
        $articleIds = array_merge($articleIds, $pinedArticleIds);

        /**
         * Select latest photo and video articles from primary category
         */
        $types = ['video'];
        if ($isRootCategory || $this->isMobile) {
            $types = ['video', 'photo'];
        }

        $latestPhotosAndVideos = $this->categoryHelper->getLatestPhotosAndVideos($rootCategory->id, $navCategoryIds, 10, $types);
        foreach ($latestPhotosAndVideos as $idx => $article) {
            $latestPhotosAndVideos[$idx]['slug'] = Scaffolding::getArticleSlug($article['metaSlug'], $article['id']);
            $imageIds[] = $article['imageId'];
        }

        /**
         * Check if category or its parents have pined event and poll
         */
        $pinedEventId = $this->category->pinedEventId;
        $categories = $this->categoryHelper->getParentCategories($this->category);
        $categories[] = $this->category;
        foreach ($categories as $category) {
            if (!empty($category->pinedEventId)) {
                $pinedEventId = $category->pinedEventId;
            }
        }

        /**
         * Load pined event
         */
        $pinedEvent = null;
        $pinedEventArticleIds = [];
        $pinedEventArticles = [];
        if (!empty($pinedEventId)) {
            $pinedEvent = Event::query()
                ->where('id=:eventId:')
                ->andWhere('status=:status:')
                ->bind(['eventId' => $pinedEventId, 'status' => 'Active'])
                ->execute()
                ->getFirst();
            if (!empty($pinedEvent)) {
                $pinedEvent = $pinedEvent->toArray();
                $pinedEvent['slug'] = Scaffolding::getEventSlug($pinedEventId, $pinedEvent['metaSlug']);
                $imageIds[] = $pinedEvent['imageId'];

                $serverConfig = $this->config->elasticSearch->toArray();
                $results = NdtElasticSearch::getInstance($serverConfig)->searchArticlesByEvent($pinedEventId, [], 0, 5);
                $pinedEventArticleIds = $results['articleIds'];
                if (empty($pinedEventArticleIds)) {
                    $pinedEventArticleIds = [];
                }
                $articleIds = array_merge($articleIds, $pinedEventArticleIds);
            }
        }

        /**
         * Query list of articles by IDs
         */
        $articleIds = array_unique($articleIds);
        $this->getArticlesByIds($articleIds, $imageIds);

        /**
         * Query list of needed images by IDs
         */
        $imageIds = array_unique($imageIds);
        $this->getImagesByIds($imageIds);

        /**
         * Get most read articles in current category
         */
        $mostReadArticles = $this->categoryHelper->getMostReadArticles($rootCategory->id, $navCategoryIds, 10);
        foreach ($mostReadArticles as $idx => $article) {
            $mostReadArticles[$idx]['slug'] = Scaffolding::getArticleSlug($article['metaSlug'], $article['id']);
        }

        /**
         * Update images for latest photo and video articles
         */
        if (!empty($latestPhotosAndVideos)) {
            foreach ($latestPhotosAndVideos as $idx => $photoOrVideo) {
                if (isset($this->imagesMap[$photoOrVideo['imageId']])) {
                    $latestPhotosAndVideos[$idx]['imageUrl'] = $this->imagesMap[$photoOrVideo['imageId']]->fileName;
                }
                else {
                    $latestPhotosAndVideos[$idx]['imageUrl'] = $this->config->defaults->articleImageUrl;
                }
            }
        }

        /**
         * Create list of pined articles
         */
        $pinedArticles = $this->extractArticlesFromMapByIds($pinedArticleIds);
        foreach ($pinedArticles as $article) {
            if (strtotime($this->lastEdited) < strtotime($article['lastEdited'])) {
                $this->lastEdited = $article['lastEdited'];
            }
        }

        /**
         * Create list of pined event articles
         */
        if (!empty($pinedEvent)) {
            $pinedEvent['imageUrl'] = $this->imagesMap[$pinedEvent['imageId']]->fileName;
            $pinedEventArticles = $this->extractArticlesFromMapByIds($pinedEventArticleIds);
        }

        /**
         * Create list of articles in current page
         */
        $articlesInPage = $this->extractArticlesFromMapByIds($pageArticleIds);

        /**
         * Set meta title trail for SEO title uniqueness
         */
        $this->metaTitleTrail = '';
        if ($page > 1) {
            $this->metaTitleTrail = ' - Trang ' . $page;
        }

        $paginationHelper = new Pagination();
        if ($this->isMobile) {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getMobileUrl(Scaffolding::getCategorySlug($this->category->metaSlug)));
        }
        else {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getCategorySlug($this->category->metaSlug)));
        }
        $paginationHelper->setTotalItems($totalArticles);
        $paginationHelper->setItemsPerPage($numArticlesPerCategoryPage);
        $paginationHelper->setCurrentPage($page);
        $pageLinks = $paginationHelper->paginate();

        $this->data = [
            'isPrimaryCategory' => $isRootCategory,
            'category' => (array) $category,
            'totalArticles' => $totalArticles,
            'page' => $page,
            'itemsPerPage' => $numArticlesPerCategoryPage,
            'mostReadArticles' => $mostReadArticles,
            'latestPhotoAndVideos' => $latestPhotosAndVideos,
            'articlesInPage' => $articlesInPage,
            'pinedArticles' => $pinedArticles,
            'subNavCategoryIds' => $navCategoryIds,
            'allCategories' => $this->categoryHelper->getAllCategories(),
            'pageLinks' => $pageLinks,

            'pinedEvent' => $pinedEvent,
            'pinedEventArticles' => $pinedEventArticles,
        ];
        $this->view->setVars($this->data);
    }

    public function setMetaTags()
    {
        $this->metaTags['MetaDescription']  = $this->category->metaDescription;
        $this->metaTags['MetaKeyword']      = $this->category->metaKeyword;
        $this->metaTags['MetaTitle']        = $this->category->metaTitle . $this->metaTitleTrail;
        $this->metaTags['MetaSlug']         = $this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getCategorySlug($this->category->metaSlug, $this->data['page']));
        $this->metaTags['PageType']         = 'Category';
        $this->metaTags['PublishTime']      = $this->category->created;
        $this->metaTags['LastEdited']       = $this->lastEdited;

        $this->metaTags['Alternates']       = [
            'handheld' => $this->helpers['urlHelper']->getMobileUrl(Scaffolding::getCategorySlug($this->category->metaSlug, $this->data['page']))
        ];

        // Generate list of breadcrumb for meta tag
        $breadcrumbList = [];
        $breadcrumbList[] = [
            'id'        => 0,
            'parentId'  => 0,
            'title'     => 'www.nguoiduatin.vn',
            'metaTitle' => 'www.nguoiduatin.vn',
            'metaSlug'  => $this->isMobile ? $this->helpers['urlHelper']->getMobileDomain() : $this->helpers['urlHelper']->getDesktopDomain()
        ];

        $parentCategories = $this->categoryHelper->getParentCategories($this->category);
        foreach ($parentCategories as $category) {
            $breadcrumbList[] = $this->categoryHelper->navCategoryData($category);
        }

        $this->metaTags['BreadcrumbList'] = $breadcrumbList;
    }

    public function setAds()
    {
        // Load ads configuration for all categories
        $commonAds = $this->globalConfigs['AdsCategory'];

        // If category has custom ads, overwrite the global configuration
        // Parent category's ads will be overwritten by child's ads
        $categoryAds = null;
        $parentCategories = $this->categoryHelper->getParentCategories($this->category);
        $parentCategories[] = $this->category;

        foreach ($parentCategories as $category) {
            if (!empty($category->adsJson)) {
                $ads = json_decode($category->adsJson);
                if (json_last_error() == JSON_ERROR_NONE) {
                    $categoryAds = $ads;
                }
            }
        }

        $ads = $commonAds ?: [];
        if (!empty($categoryAds)) {
            foreach ($categoryAds as $configKey => $configValue) {
                $ads[$configKey] = $configValue;
            }
        }

        $this->ads = $ads;
    }
}