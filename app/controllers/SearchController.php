<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\NdtElasticSearch;
use Ixosoftware\Cms\Helpers\Pagination;
use Ixosoftware\Cms\Helpers\Scaffolding;

class SearchController extends ControllerBase
{
    private $data = [];
    private $lastEdited;

    public function indexAction()
    {
        $query = $this->request->get('q');
        $q = str_replace('\'', '\\\'', $query);

        $page  = intval($this->request->get('p'));
        if (!empty($page) && $page == 1) {
            return $this->response->redirect('/search?q=' . $q);
        }
        if (!$page) {
            $page = 1;
        }

        $itemsPerPage = $this->config->defaults->numSearchResultsPerPage;
        $from = ($page - 1) * $itemsPerPage;

        $serverConfig = $this->config->elasticSearch->toArray();
        $response = NdtElasticSearch::getInstance($serverConfig)->searchArticles($q, $from, $itemsPerPage);

        $total = $response['total'];
        $hits = $response['hits'];

        $totalPages = ceil($total / $itemsPerPage);

        $articleIds = [];
        if (!empty($hits)) {
            foreach ($hits as $hit) {
                if (isset($hit['_source']['id'])) {
                    array_push($articleIds, $hit['_source']['id']);
                } else {
                    array_push($articleIds, $hit['_id']);
                }
            }
        }

        $imagesIds = [];
        $this->getArticlesByIds($articleIds, $imagesIds);
        $this->getImagesByIds($imagesIds);

        $articles = [];
        foreach ($articleIds as $articleId) {
            if (isset($this->articlesMap[$articleId])) {
                $article = $this->articlesMap[$articleId]->toArray();
                if (isset($this->imagesMap[$article['imageId']])) {
                    if ($this->isMobile) {
                        $article['imageUrl'] = $this->helpers['imageHelper']->generateThumbLink($this->imagesMap[$article['imageId']]->fileName, 156, 117);
                    }
                    else {
                        $article['imageUrl'] = $this->helpers['imageHelper']->generateThumbLink($this->imagesMap[$article['imageId']]->fileName, 320, 198);
                    }
                }
                else {
                    $article['imageUrl'] = $this->config->defaults->articleImageUrl;
                }

                if (!$this->lastEdited || strtotime($article['lastEdited']) > $this->lastEdited) {
                    $this->lastEdited = strtotime($article['lastEdited']);
                }

                $articles[] = $article;
            }
        }

        $paginationHelper = new Pagination();
        if ($this->isMobile) {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getMobileUrl('/search'));
        }
        else {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getDesktopUrl('/search'));
        }
        $paginationHelper->setTotalItems($total);
        $paginationHelper->setItemsPerPage($itemsPerPage);
        $paginationHelper->setCurrentPage($page);
        $paginationHelper->setOtherParams(['q' => $query]);
        $paginationHelper->setPageInQuery(true);
        $paginationHelper->setPageSegmentName('p');
        $pageLinks = $paginationHelper->paginate();

        $this->data = [
            'articles' => $articles,
            'searchQuery' => $query,
            'totalResults' => $total,
            'totalPages' => $totalPages,
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
            'pageLinks' => $pageLinks
        ];

        $this->view->setVars($this->data);
    }

    function setMetaTags()
    {
        $description = 'Kết quả tìm kiếm cho "' . $this->data['searchQuery'] . '"';
        if ($this->data['totalResults'] > 0) {
            $description .= ' - Tìm thấy ' . $this->data['totalResults'] . ' kết quả.';
        }
        $this->metaTags['MetaDescription']  = $description;
        $this->metaTags['MetaKeyword']      = '';

        $appendedTitle = '';
        if ($this->data['totalResults'] > $this->data['itemsPerPage'] && $this->data['page'] > 1) {
            $appendedTitle = ' - Trang ' . $this->data['page'];
        }
        $this->metaTags['MetaTitle']        = 'Kết quả tìm kiếm cho ' . $this->data['searchQuery'] . $appendedTitle;

        $this->metaTags['MetaSlug']         = '';
        $this->metaTags['PageType']         = 'Search';
        $this->metaTags['PublishTime']      = date('c', $this->lastEdited);
        $this->metaTags['LastEdited']       = date('c', $this->lastEdited);
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}