<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\NdtElasticSearch;
use Ixosoftware\Cms\Helpers\Pagination;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Models\Tag;

class TagController extends ControllerBase
{
    private $tag;
    private $lastEdited;
    private $data = [];

    public function indexAction()
    {
        $slug = $this->dispatcher->getParam('slug');
        if (empty($slug)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Trang không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }

        $tag = Tag::query()
            ->where('metaSlug = :metaSlug:')
            ->andWhere('status=:status:')
            ->bind(['metaSlug' => $slug, 'status' => 'Active'])
            ->execute()
            ->getFirst();
        if (empty($tag) || empty($tag->title)) {
            throw new \Phalcon\Mvc\Dispatcher\Exception('Trang không tồn tại', \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND);
        }
        $this->tag = $tag;

        $page = intval($this->dispatcher->getParam('page'));
        if ((empty($page) || $page <= 1) && strpos($this->router->getRewriteUri(), '/page') !== false) {
            return $this->response->redirect('/tag/' . $slug);
        }
        if (!$page) { $page = 1; }

        $itemsPerPage = $this->config->defaults->numArticlesPerTagPage;
        $from = ($page - 1) * $itemsPerPage;

        $imageIds = [];

        $serverConfig = $this->config->elasticSearch->toArray();
        $results = NdtElasticSearch::getInstance($serverConfig)->searchArticlesByTag($this->tag->id, $from, $itemsPerPage);

        $total = $results['total'];
        $this->lastEdited = strtotime($tag->lastEdited);

        $articleIds = $results['articleIds'];
        $this->getArticlesByIds($articleIds, $imageIds);
        $this->getImagesByIds($imageIds);

        $articles = $this->extractArticlesFromMapByIds($articleIds, $this->lastEdited);

        $tagSlug = Scaffolding::getTagSlug($this->tag->metaSlug);
        $paginationHelper = new Pagination();
        if ($this->isMobile) {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getMobileUrl($tagSlug));
        }
        else {
            $paginationHelper->setBaseUrl($this->helpers['urlHelper']->getDesktopUrl($tagSlug));
        }
        $paginationHelper->setTotalItems($total);
        $paginationHelper->setItemsPerPage($itemsPerPage);
        $paginationHelper->setCurrentPage($page);
        $paginationHelper->setPageSegmentName('page');
        $pageLinks = $paginationHelper->paginate();

        $this->data = [
            'articles' => $articles,
            'tag' => $this->tag->toArray(),
            'totalArticles' => $total,
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
            'tagLink' => Scaffolding::getTagSlug($this->tag->metaSlug, $page),
            'pageLinks' => $pageLinks
        ];

        // Show anchor text under article title
        if (!empty($this->tag->anchorText) && !empty($this->tag->anchorTextActive)) {
            $this->data['anchorText'] = $this->event->anchorText;
            if ($this->isMobile) {
                $this->data['anchorTextLink'] = $this->helpers['urlHelper']->getMobileUrl(Scaffolding::getTagSlug($this->tag->id, $this->tag->metaSlug));
            }
            else {
                $this->data['anchorTextLink'] = $this->helpers['urlHelper']->getDesktopUrl(Scaffolding::getTagSlug($this->tag->id, $this->tag->metaSlug));
            }
        }

        $this->view->setVars($this->data);
    }

    function setMetaTags()
    {
        if (empty($this->tag->metaTitle)) {
            $tagTitle = ucfirst($this->tag->title . ' - Tin Tức về ' . $this->tag->title . ' mới nhất');
        }
        else {
            $tagTitle = $this->tag->metaTitle;
        }

        $appendedTitle = '';
        if ($this->data['totalArticles'] >= $this->data['itemsPerPage'] && $this->data['page'] > 1) {
            $appendedTitle .= ' - Trang ' . $this->data['page'];
        }

        if (empty($this->tag->description)) {
            $tagDescription = ucfirst($this->tag->title . ' - Tin tức ' . $this->tag->title . ' cập nhật đầy đủ và mới nhất trên Báo Người Đưa Tin.');
        }
        else {
            $tagDescription = ucfirst($this->tag->description);
        }

        if ($this->isMobile) {
            $tagLink = $this->helpers['urlHelper']->getMobileUrl($this->data['tagLink']);
        }
        else {
            $tagLink = $this->helpers['urlHelper']->getDesktopUrl($this->data['tagLink']);
        }

        $this->metaTags['MetaTitle']        = $tagTitle . $appendedTitle;
        $this->metaTags['MetaDescription']  = $tagDescription;
        $this->metaTags['MetaKeyword']      = $this->tag->metaKeyword;
        $this->metaTags['MetaSlug']         = $tagLink;
        $this->metaTags['PageType']         = 'Tag';
        $this->metaTags['PublishTime']      = $this->tag->created;
        $this->metaTags['LastEdited']       = date('Y-m-d H:i:s', $this->lastEdited);
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}