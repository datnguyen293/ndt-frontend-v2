<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\Category as CategoryHelper;
use Ixosoftware\Cms\Models\AnonymousUser;
use Ixosoftware\Cms\Models\Comment;
use Ixosoftware\Cms\Models\User;
use Phalcon\Exception;

class CommentController extends ControllerBase
{
    private $categoryHelper;

    public function initialize()
    {
        parent::initialize();
        $this->categoryHelper = new CategoryHelper();
    }

    public function articleCommentsAction()
    {
        $articleId = $this->dispatcher->getParam('articleId');
        $page = intval($this->request->get('page'));
        $sort = $this->request->get('sort');

        $itemsPerPage = $this->config->defaults->numCommentsPerPage;
        if (empty($page) || $page < 1) {
            $page = 1;
        }
        $from = ($page - 1) * $itemsPerPage;

        $orderBy = 'created asc';
        if (!empty($sort) && $orderBy == 'newest') {
            $orderBy = 'created desc';
        }

        $commentIds = [];
        $commentsMap = [];

        // Query all comments
        $result = Comment::query()
            ->where('articleId=:articleId:')
            ->andWhere('status=:status:')
            ->andWhere('parentId=0')
            ->orderBy($orderBy)
            ->limit($itemsPerPage, $from)
            ->bind(['articleId' => $articleId, 'status' => 'Publish'])
            ->execute();
        if ($result->count() > 0) {
            foreach ($result as $comment) {
                $commentIds[] = $comment->id;

                $commentArr = $comment->toArray();
                $commentArr['replies'] = [];
                $commentsMap[$comment->id] = $commentArr;
            }
        }

        // Query all sub-comments
        if (!empty($commentIds)) {
            $result = Comment::query()
                ->inWhere('parentId', $commentIds)
                ->andWhere('status=\'Publish\'')
                ->orderBy($orderBy)
                ->execute();
            if ($result->count()) {
                foreach ($result as $comment) {
                    $commentsMap[$comment->parentId]['replies'][] = $comment->toArray();
                }
            }
        }

        $comments = [];
        foreach ($commentIds as $commentId) {
            $comments[] = $commentsMap[$commentId];
        }

        $data = [
            'comments' => $comments,
            'page' => $page,
            'itemsPerPage' => $itemsPerPage
        ];

        return $this->sendJsonResponse([
            'code' => 200,
            'message' => 'Thành công',
            'data' => $data
        ]);
    }


    public function postCommentAction()
    {
        $articleId = $this->dispatcher->getParam('articleId');
        $requestJson = $this->request->getJsonRawBody();

        if (empty($articleId)) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Lỗi gửi dữ liệu',
                'message' => 'Thiếu thông tin bài viết.'
            ]);
        }

        if (empty($requestJson)) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Lỗi gửi dữ liệu',
                'message' => 'Thiếu thông tin bình luận.'
            ]);
        }

        /**
         * Check captcha if required
         */
        if (!empty($this->config->comment->enabledCommentCaptcha)) {
            if (empty($requestJson->captcha) || !$this->captchaHelper->testCaptcha($requestJson->captcha)) {
                return $this->sendJsonResponse([
                    'code' => 400,
                    'title' => 'Thông báo lỗi',
                    'message' => 'Mã bảo mật không đúng. Vui lòng kiểm tra lại.'
                ]);
            }
        }

        /**
         * Check if user is logged in with Facebook
         */
        $user = null;
        if (!empty($requestJson->socialAccessToken)) {
            $fb = new \Facebook\Facebook([
                'app_id' => $this->config->comment->facebook->appId,
                'app_secret' => $this->config->comment->facebook->appSecret,
                'default_graph_version' => 'v2.9',
            ]);
            try {
                $response = $fb->get('/me?locale=en_US&fields=id,name,email,picture.width(40).height(40)', $requestJson->socialAccessToken);
                $me = $response->getGraphUser();
                if (!empty($me)) {
                    $facebookId = $me->getId();
                    $user = User::query()
                        ->where('facebookId=\''.$facebookId.'\'')
                        ->andWhere('status=\'Active\'')
                        ->execute()
                        ->getFirst();

                    if (empty($user)) {
                        $user = new User();
                        $user->displayName = $me->getName();
                        $user->email = $me->getEmail();
                        $user->facebookId = $facebookId;
                        $user->facebookToken = $requestJson->socialAccessToken;
                        $user->avatar = $me->getPicture() ? $me->getPicture()->getUrl() : null;
                        $user->status = 'Active';
                        $user->lastEdited = date('Y-m-d H:i:s');
                        $user->created = date('Y-m-d H:i:s');
                        $user->failedLoginCount = 0;
                        $user->lockedOutUntil = date('Y-m-d H:i:s', strtotime('+20 years'));
                        $user->save();
                    }
                }
            }
            catch (Exception $e) {
                return $this->sendJsonResponse([
                    'code' => 400,
                    'title' => 'Thông báo lỗi',
                    'message' => 'Không thể đăng nhập với Facebook, vui lòng thử lại.'
                ]);
            }
        }

        /**
         * Create anonymous user
         */
        $anonymousUser = null;
        if (empty($user)) {
            if (!empty($requestJson->userInfo)) {
                $anonymousUser = new AnonymousUser();
                $anonymousUser->fullName = $requestJson->userInfo->name;
                $anonymousUser->Email = $requestJson->userInfo->email;
                $anonymousUser->{'User-Agent'} = $this->request->getUserAgent();
                $anonymousUser->Referrer = $this->request->getHTTPReferer();
                $anonymousUser->IP = $this->request->getClientAddress();

                $anonymousUser->save();
            }
            else {
                return $this->sendJsonResponse([
                    'code' => 400,
                    'title' => 'Thông báo lỗi',
                    'message' => 'Vui lòng nhập thông tin của bạn.'
                ]);
            }
        }

        if (empty($requestJson->content) || str_word_count($requestJson->content) < $this->config->comment->minCommentLengthWords) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Bình luận quá ngắn',
                'message' => 'Bạn chưa nhập nội dung bình luận hoặc bình luận của bạn quá ngắn. Nội dung bình luận có nghĩa sẽ được đánh giá cao và ưu tiên hơn khi duyệt và hiển thị.'
            ]);
        }

        /**
         * Check last comment time
         */
        $lastCommentTime = $this->session->get('USER_LAST_COMMENT_TIME');
        if (!empty($lastCommentTime)) {
            $allowedCommentIntervalSeconds = $this->config->comment->allowedCommentIntervalSeconds;
            $eslapsedTime = time() - $lastCommentTime;
            if ($eslapsedTime < $allowedCommentIntervalSeconds) {
                return $this->sendJsonResponse([
                    'code' => 400,
                    'title' => 'Thông báo',
                    'message' => sprintf('Xin hãy đợi %ds để tiếp tục bình luận.', ($allowedCommentIntervalSeconds - $eslapsedTime))
                ]);
            }
        }

        $parentId = 0;
        if (!empty($requestJson->parentId)) {
            $parentId = $requestJson->parentId;
        }

        $comment = new Comment();
        $comment->articleId = $articleId;
        $comment->content = $requestJson->content;
        $comment->parentId = $parentId;
        $comment->status = 'New';
        $comment->lastEdited = date('Y-m-d H:i:s');
        $comment->created = date('Y-m-d H:i:s');
        $comment->ip = $this->request->getClientAddress();
        $comment->userAgent = $this->request->getUserAgent();
        $comment->anonymousUserId = !empty($anonymousUser) ? $anonymousUser->id : null;
        $comment->userId = !empty($user) ? $user->id : null;

        if ($comment->save() === false) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Lỗi xảy ra',
                'message' => 'Bình luận của bạn không được lưu do hệ thống của chúng tôi đang quá tải. Chân thành cáo lỗi và cảm ơn sự quan tâm của quý đọc giả.'
            ]);
        }
        else {
            /**
             * Set last comment time
             */
            $this->session->set('USER_LAST_COMMENT_TIME', time());

            return $this->sendJsonResponse([
                'code' => 200,
                'title' => 'Thành công',
                'message' => 'Bình luận của bạn đã được gửi đi',
                'data' => $comment->toJson()
            ]);
        }
    }

    public function captchaAction()
    {
        $this->view->disable();

        $this->response->setContentType('image/jpeg');
        $this->captchaHelper->generateNewCaptcha();
        $this->response->send();
        return false;
    }

    public function likeCommentAction()
    {
        $articleId = $this->dispatcher->getParam('articleId');
        $commentId = $this->dispatcher->getParam('commentId');

        if (empty($commentId)) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Thông báo lỗi',
                'message' => 'Lỗi dữ liệu gửi lên.'
            ]);
        }

        $sessionKey = sprintf('like_comment_%s', $commentId);
        $sessionLastLikeKey = sprintf('last_time_like_%s', $commentId);

        $likes24h = $this->session->get($sessionKey);
        $lastTimeLike = (int) $this->session->get($sessionLastLikeKey);

        $timeDiff = -24;
        $totalCount = 0;
        $count24h = 0;

        if (!empty($likes24h)) {
            list($count24h, $totalCount) = explode('|', $likes24h);
        }

        if (!empty($lastTimeLike)) {
            $timeDiff = (time() - $lastTimeLike) / 3600;
        }

        if ($timeDiff >= 24) {
            $count24h = 1;
            $totalCount += 1;
            $this->session->set($sessionKey, $count24h . '|' . $totalCount);
            $this->session->set($sessionLastLikeKey, time());
        }
        else if ($count24h < 3) {
            $count24h += 1;
            $totalCount += 1;
            $this->session->set($sessionKey, $count24h . '|' . $totalCount);
        }
        else {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Thông báo lỗi',
                'message' => 'Bạn đã sử dụng chức năng này quá 3 lần cho phép. Vui lòng đợi 24h để được thao tác tiếp.'
            ]);
        }

        $comment = Comment::findFirst('id='.(int)$commentId);
        if (empty($comment)) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Thông báo lỗi',
                'message' => 'Không tìm thấy bình luận.'
            ]);
        }

        if ($totalCount % 2 == 0) {
            $comment->likeCount -= 1;
        }
        else {
            $comment->likeCount += 1;
        }

        if ($comment->save() === false) {
            return $this->sendJsonResponse([
                'code' => 400,
                'title' => 'Thông báo lỗi',
                'message' => 'Có lỗi xảy ra, vui lòng thử lại.'
            ]);
        }

        return $this->sendJsonResponse([
            'code' => 200,
            'title' => 'Thành công',
            'message' => 'Thành công'
        ]);
    }


    function setMetaTags()
    {
        // TODO: Implement setMetaTags() method.
    }

    function setAds()
    {
        // TODO: Implement setAds() method.
    }
}