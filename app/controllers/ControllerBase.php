<?php
namespace Ixosoftware\Cms\Controllers;

use Ixosoftware\Cms\Helpers\ComplexSearch;
use Ixosoftware\Cms\Helpers\Date as DateHelper;
use Ixosoftware\Cms\Helpers\Image as ImageHelper;
use Ixosoftware\Cms\Helpers\Scaffolding;
use Ixosoftware\Cms\Helpers\URL as URLHelper;
use Ixosoftware\Cms\Models\Article;
use Ixosoftware\Cms\Models\Configuration;
use Ixosoftware\Cms\Models\File;
use Ixosoftware\Cms\Models\Menu;
use Phalcon\Dispatcher;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

abstract class ControllerBase extends Controller
{
    protected $config = [];
    protected $language = null;
    protected $theme = null;
    protected $layout = DESKTOP_LAYOUT;
    protected $isMobile = false;
    protected $helpers = [];

    protected $globalConfigs = [];
    protected $metaTags = [];
    protected $ads = [];

    protected $articlesMap = [];
    protected $imagesMap = [];

    public function initialize()
    {
        $this->config = $this->di->getShared('config');
        $this->loadDefaultLanguage();
        $this->loadTheme();
        $this->loadLayout();
        $this->loadHelpers();
        $this->loadCommonData();
    }

    abstract function setMetaTags();
    abstract function setAds();

    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Set meta tags to view
        $this->metaTags['MainUrl'] = ($this->isMobile ? $this->helpers['urlHelper']->getCurrentMobileUrl() : $this->helpers['urlHelper']->getCurrentDesktopUrl());
        $this->metaTags['RootUrl'] = ($this->isMobile ? $this->helpers['urlHelper']->getMobileDomain() : $this->helpers['urlHelper']->getDesktopDomain());
        $this->metaTags['CurrentUrl'] = $this->metaTags['MainUrl'];
        $this->metaTags['LogoUrl'] = $this->config->defaults->logo;

        $this->setMetaTags();

        $this->view->setVar('metaTags', $this->metaTags);

        // Set ads to view
        $this->setAds();
        $this->view->setVar('ads', $this->ads);
    }

    /**
     * Load static data for whole pages
     */
    protected function loadCommonData()
    {
        $data = [];

        // Load menus
        $menus = $this->loadMenus();

        // Load footer configuration
        $this->globalConfigs = $this->loadConfiguration();

        $data['config'] = $this->config;

        $this->view->setVars([
            'config' => $this->config,
            'menus' => $menus,
            'configurations' => $this->globalConfigs
        ]);
    }

    protected function sendJsonResponse($data = [])
    {
        $response = new Response();
        $response->setJsonContent($data);

        return $response;
    }

    private function loadMenus()
    {
        $cachedKey = 'All_Menus';
        $menus = $this->getDI()->getShared('memcached')->get($cachedKey);
        if (!empty($menus)) {
            return $menus;
        }

        $menus = [];
        $result = Menu::query()
            ->where('status = \'Active\'')
            ->execute();
        if ($result->count()) {
            foreach ($result as $menu) {
                $positions = explode(',', $menu->position);
                foreach ($positions as $position) {
                    $menus[$position] = json_decode($menu->content, true);
                }
            }

            // Cache menu 6 hours
            $this->getDI()->getShared('memcached')->save($cachedKey, $menus, 6*60*60);
        }

        return $menus;
    }

    private function loadConfiguration()
    {
        $cachedKey = 'All_Configurations';
        $configs = $this->getDI()->getShared('memcached')->get($cachedKey);
        if (!empty($configs)) {
            return $configs;
        }

        $configs = [];
        $result = Configuration::query()
            ->where('configType IS NOT NULL')
            ->andWhere('status = \'Active\'')
            ->execute();
        if ($result->count()) {
            foreach ($result as $config) {
                $configs[$config->configType] = json_decode($config->jsonConfig, true);
                if (json_last_error() != JSON_ERROR_NONE) {
                    $configs[$config->configType] = $config->jsonConfig;
                }
            }

            // Cache all configuration for 2 hours
            $this->getDI()->getShared('memcached')->save($cachedKey, $configs, 2*60*60);
        }

        return $configs;
    }

    protected function getArticlesByIds($articleIds = [], &$imageIds = [])
    {
        $this->articlesMap = [];
        $result = Article::query()
            ->columns('id, title, description, metaSlug, metaTitle, imageId, lastEdited, contentType, allowComment, showComment, commentCount, sapo, publishTime')
            ->where('status = \'Publish\'')
            ->andWhere('stage = \'Publish\'')
            ->inWhere('id', $articleIds)
            ->execute();
        if ($result->count()) {
            foreach ($result as $article) {
                $article->slug = Scaffolding::getArticleSlug($article->metaSlug, $article->id);
                $this->articlesMap[$article->id] = $article;
                $imageIds[] = $article->imageId;
            }
        }
    }

    protected function getImagesByIds($imageIds = [])
    {
        $this->imagesMap = [];
        $imageIds = array_unique($imageIds);
        $result = File::query()
            ->inWhere('id', $imageIds)
            ->execute();
        if ($result->count()) {
            foreach ($result as $file) {
                $this->imagesMap[$file->id] = $file;
            }
        }
    }

    protected function extractArticlesFromMapByIds($articleIds, &$lastEdited = null)
    {
        $articles = [];

        foreach ($articleIds as $articleId) {
            if (isset($this->articlesMap[$articleId])) {
                $article = $this->articlesMap[$articleId];

                if (isset($this->imagesMap[$article->imageId])) {
                    $article->imageUrl = $this->imagesMap[$article->imageId]->fileName;
                }
                else {
                    $article->imageUrl = $this->config->defaults->articleImageUrl;
                }

                if ($lastEdited == null || $lastEdited < strtotime($article->lastEdited)) {
                    $lastEdited = strtotime($article->lastEdited);
                }

                $articles[] = $article->toArray();
            }
        }

        return $articles;
    }

    /**
     * Use the default language in configuration file (config.php)
     */
    protected function loadDefaultLanguage()
    {
        $this->language = $this->config->languages->default;
    }

    /**
     * Set theme name from config.php file
     */
    protected function loadTheme()
    {
        $this->theme = $this->config->theme;
    }

    /**
     * Set layout based on user-agent
     * This function is powered by https://github.com/serbanghita/Mobile-Detect/ library
     */
    protected function loadLayout()
    {
        $this->layout = DESKTOP_LAYOUT;

        $detector = new \Mobile_Detect();
        if ($detector->isMobile()) {
            $this->isMobile = true;
            $this->layout = MOBILE_LAYOUT;
            $this->view->setLayout('mobile.layout');
        }
        else {
            $this->view->setLayout('desktop.layout');
        }

        $this->pickDefaultView();
    }

    private function pickDefaultView()
    {
        $controllerName = $this->dispatcher->getControllerName();
        $actionName = $this->dispatcher->getActionName();

        if ($this->isMobile) {
            $this->view->pick(Scaffolding::getViewName($controllerName . '/m.' . $actionName));
        }
        else {
            $this->view->pick(Scaffolding::getViewName($controllerName . '/' . $actionName));
        }
    }

    /**
     * Load all helpers classes
     */
    protected function loadHelpers()
    {
        $this->helpers = [
            'imageHelper' => new ImageHelper(),
            'complexSearchHelper' => new ComplexSearch(),
            'urlHelper' => new URLHelper(),
            'dateHelper' => new DateHelper(),
        ];
        $this->view->setVars($this->helpers);
    }
}
