<?php
namespace Ixosoftware\Cms\Controllers;

class ErrorHandlerController extends ControllerBase
{
    public function indexAction()
    {
        $this->response->setStatusCode(400);
        $this->response->setContent('Page not found');
        return $this->response;
    }

    public function notFoundAction()
    {
        $this->response->redirect('/');
        return false;
    }

    public function setMetaTags()
    {
        // TODO: Implement metaTags() method.
    }

    public function setAds()
    {
        // TODO: Implement setAds() method.
    }
}
